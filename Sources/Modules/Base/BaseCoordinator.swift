//
//  BaseCoordinator.swift
//  BLETest
//

import Foundation

protocol BaseCoordinatorDelegate: class {
  func coordinatorRootViewControllerDidDeinit(_ coordinator: BaseCoordinator)
}

protocol BaseCoordinator: BaseCoordinatorDelegate {
  var childCoordinators: [BaseCoordinator] { get set }
  var baseDelegate: BaseCoordinatorDelegate? { get set }
  
  func start()
}

extension BaseCoordinator {
  func remove(child coordinator: BaseCoordinator) {
    if let index = childCoordinators.index(where: { $0 === coordinator }) {
      childCoordinators.remove(at: index)
    }
  }
  
  func addChildCoordinator(_ coordinator: BaseCoordinator) {
    childCoordinators.append(coordinator)
    coordinator.baseDelegate = self
  }
  
  func coordinatorRootViewControllerDidDeinit(_ coordinator: BaseCoordinator) {
    remove(child: coordinator)
  }
}
