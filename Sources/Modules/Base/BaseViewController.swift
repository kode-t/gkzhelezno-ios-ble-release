//
//  BaseViewController.swift
//  BLETest
//

import UIKit

class BaseViewController: UIViewController {
  
  // MARK: - Life cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureNavigationBar()
  }
  
  // MARK: - Customize navigation bar
  
  func configureNavigationBar() {
    removeBackButtonTitle()
  }
}
