//
//  MainCoordinator.swift
//  BLETest
//

import UIKit
import BluetoothService

class MainCoordinator: BaseCoordinator {
  
  // MARK: - Properties
  
  var childCoordinators: [BaseCoordinator] = []
  weak var baseDelegate: BaseCoordinatorDelegate?

  private var window: UIWindow
  private var rootNavigationController = UINavigationController()
  private let bluetoothService: BluetoothServiceProtocol
  private let userDefaultsDataStore: UserDefaultsDataStore
  private let bluetoothServiceEventHandler: BluetoothServiceEventHandler
  private var ignoreAlerts = false

  // MARK: - Init
  
  required init(window: UIWindow,
                bluetoothService: BluetoothServiceProtocol,
                bluetoothServiceEventHandler: BluetoothServiceEventHandler,
                userDefaultsDataStore: UserDefaultsDataStore) {
    self.window = window
    self.bluetoothService = bluetoothService
    self.bluetoothServiceEventHandler = bluetoothServiceEventHandler
    self.userDefaultsDataStore = userDefaultsDataStore
    bluetoothServiceEventHandler.delegate = self
    bluetoothService.delegates.add(self)
    window.rootViewController = rootNavigationController
    configureNavigationBarAppearance()
  }

  // MARK: - Navigation

  func start() {
    showManualGateOpeningScreen()
  }
  
  func goBackToManualGateOpeningScreen() {
    rootNavigationController.presentedViewController?.dismiss(animated: false, completion: nil)
    rootNavigationController.popToRootViewController(animated: false)
  }

  private func showManualGateOpeningScreen() {
    let coordinator = ManualGateOpeningCoordinator(navigationController: rootNavigationController,
                                                   bluetoothService: bluetoothService,
                                                   bluetoothServiceEventHandler: bluetoothServiceEventHandler,
                                                   userDefaultsDataStore: userDefaultsDataStore)
    addChildCoordinator(coordinator)
    coordinator.start()
  }
  
  private func showBluetoothReminderOverlay() {
    let bluetoothOverlay = childCoordinators.filter { $0 is BluetoothReminderOverlayCoordinator }
    if !bluetoothOverlay.isEmpty {
      return
    }
    
    let coordinator = BluetoothReminderOverlayCoordinator(navigationController: rootNavigationController,
                                                          bluetoothService: bluetoothService)
    addChildCoordinator(coordinator)
    coordinator.start()
  }

  // MARK: - Show Alert
  
  private func showAlertWith(title: String,
                             text: String,
                             showCancelButton: Bool = true,
                             okButtonName: String = "ОК",
                             okActionHandler: (() -> Void)? = nil) {
    rootNavigationController.showAlert(withTitle: title, message: text, showCancelButton: showCancelButton,
                                       okButtonTitle: okButtonName,
                                       tapBlock: okActionHandler, cancelTapBlock: nil)
  }
  
  private func showGateOpeningErrorAlert(tryAgain: Bool = false) {
    ignoreAlerts = true
    var okButtonName: String = "ОК"
    if tryAgain {
      okButtonName = "Повторить попытку"
    }
    showAlertWith(title: "BLE Test",
                  text: "Произошла ошибка",
                  showCancelButton: tryAgain,
                  okButtonName: okButtonName) {
      self.bluetoothService.connectAndOpenGate()
    }
  }
  
  // MARK: - Appearance
  
  private func configureNavigationBarAppearance() {
    rootNavigationController.navigationBar.isTranslucent = false
    rootNavigationController.navigationBar.tintColor = UIColor.black
    rootNavigationController.navigationBar.barTintColor = UIColor.white
    rootNavigationController.navigationBar.shadowImage = UIImage()
  }
}

// MARK: - BluetoothServiceEventHandler Delegate

extension MainCoordinator: BluetoothServiceEventHandlerDelegate {
  func bluetoothServiceEventHandlerRequestsOpenGateManually(_ bluetoothServiceEventHandler: BluetoothServiceEventHandler) {
    guard !ignoreAlerts, !(rootNavigationController.topViewController is ManualGateOpeningViewController) else { return }
    ignoreAlerts = true
    showAlertWith(title: "BLE Test",
                  text: "Обнаружено устройство. Открыть?",
                  okButtonName: "Открыть") {
      self.bluetoothService.connectAndOpenGate()
    }
  }
}

// MARK: - BluetoothService Delegate

extension MainCoordinator: BluetoothServiceDelegate {
  func bluetoothServiceDidDetectBluetoothOff(_ bluetoothService: BluetoothServiceProtocol) {
    showBluetoothReminderOverlay()
  }
  
  func bluetoothServiceDidLoseGate(_ bluetoothService: BluetoothServiceProtocol) {
    if let presentedController = rootNavigationController.presentedViewController,
      presentedController is UIAlertController {
      presentedController.dismiss(animated: true, completion: nil)
    }
  }
  
  func bluetoothServiceDidOpenGate(_ bluetoothService: BluetoothServiceProtocol) {
    ignoreAlerts = false
  }
  
  func bluetoothServiceDidChangeSignalPower(_ bluetoothService: BluetoothServiceProtocol) {
    userDefaultsDataStore.signalPowerChanged = false
  }
  
  func bluetoothServiceDidChangeTimeoutInterval(_ bluetoothService: BluetoothServiceProtocol) {
    userDefaultsDataStore.advertisingTimeoutChanged = false
  }
  
  func bluetoothServiceDidReceiveConnectionError(_ bluetoothService: BluetoothServiceProtocol) {
    showGateOpeningErrorAlert()
  }
  
  func bluetoothServiceDidFailToOpenGate(_ bluetoothService: BluetoothServiceProtocol) {
    showGateOpeningErrorAlert(tryAgain: true)
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol, didReadTimeoutInterval timeout: Int) {
    userDefaultsDataStore.advertisingTimeout = bluetoothService.advertisingTimeout
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol, didReadSignalPower signalPowerIndex: Int) {
    userDefaultsDataStore.signalPowerIndex = bluetoothService.signalPowerIndex
  }
}
