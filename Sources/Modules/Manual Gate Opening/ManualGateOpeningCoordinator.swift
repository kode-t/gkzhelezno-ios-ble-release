//
//  ManualGateOpeningCoordinator.swift
//  BLETest
//
//

import UIKit
import BluetoothService

class ManualGateOpeningCoordinator: BaseCoordinator {
  
  // MARK: - Properties
  var childCoordinators: [BaseCoordinator] = []
  weak var baseDelegate: BaseCoordinatorDelegate?
  
  private let navigationController: UINavigationController
  private let bluetoothService: BluetoothServiceProtocol
  private let bluetoothServiceEventHandler: BluetoothServiceEventHandler
  private let userDefaultsDataStore: UserDefaultsDataStore
  
  // MARK: - Init
  
  required init(navigationController: UINavigationController,
                bluetoothService: BluetoothServiceProtocol,
                bluetoothServiceEventHandler: BluetoothServiceEventHandler,
                userDefaultsDataStore: UserDefaultsDataStore) {
    self.navigationController = navigationController
    self.bluetoothService = bluetoothService
    self.bluetoothServiceEventHandler = bluetoothServiceEventHandler
    self.userDefaultsDataStore = userDefaultsDataStore
  }

  // MARK: - Methods
  
  func start() {
    showManualGateOpeningScreen()
  }
  
  private func showManualGateOpeningScreen() {
    let viewController = ManualGateOpeningViewController(bluetoothService: bluetoothService,
                                                         userDefaultsDataStore: userDefaultsDataStore)
    viewController.title = "Ручное открытие"
    viewController.delegate = self
    viewController.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "settings_icon"), style: .plain, target: self,
                                                                       action: #selector(showSettings(_:)))
    navigationController.pushViewController(viewController, animated: false)
  }
  
  @objc private func showSettings(_ sender: UIBarButtonItem?) {
    let settingsCoordinator = SettingsCoordinator(navigationController: navigationController,
                                                  bluetoothService: bluetoothService,
                                                  bluetoothServiceEventHandler: bluetoothServiceEventHandler,
                                                  userDefaultsDataStore: userDefaultsDataStore)
    settingsCoordinator.baseDelegate = self
    addChildCoordinator(settingsCoordinator)
    settingsCoordinator.start()
  }
}

extension ManualGateOpeningCoordinator: ManualGateOpeningViewControllerDelegate {
  func manualGateOpeningVCDidFinish(manualGateOpeningVC: ManualGateOpeningViewController) {
    baseDelegate?.coordinatorRootViewControllerDidDeinit(self)
  }
}
