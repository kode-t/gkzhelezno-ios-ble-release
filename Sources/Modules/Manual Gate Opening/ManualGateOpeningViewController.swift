//
//  ManualGateOpeningViewController.swift
//  BLETest
//
//

import UIKit
import BluetoothService

protocol ManualGateOpeningViewControllerDelegate: class {
  func manualGateOpeningVCDidFinish(manualGateOpeningVC: ManualGateOpeningViewController)
}

private extension Constants {
  static let maxGateOpeningTime: TimeInterval = 5.0
}

class ManualGateOpeningViewController: BaseViewController {
  // MARK: - Outlets

  @IBOutlet private weak var showLogsButton: UIButton!
  @IBOutlet private weak var pushButtonLabel: UILabel!
  @IBOutlet private weak var noGateDetectedView: UIView!
  @IBOutlet private weak var moveCloserToGateLabel: UILabel!
  @IBOutlet private weak var noGateDetectedLabel: UILabel!
  @IBOutlet private weak var searchingView: UIView!
  @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
  @IBOutlet private weak var searchingLabel: UILabel!
  @IBOutlet private weak var openGatesButton: UIButton! {
    didSet {
      openGatesButton.setImage(#imageLiteral(resourceName: "open_gate_pressed"), for: [.selected, .highlighted])
      openGatesButton.setImage(#imageLiteral(resourceName: "open_gate_disabled"), for: .disabled)
    }
  }
  private let logsView: LogsView = LogsView()
  private var openingGateTimer: Timer?
  private let bluetoothService: BluetoothServiceProtocol
  private let userDefaultsDataStore: UserDefaultsDataStore
  private var isSearching = true {
    didSet {
      if isSearching == true {
        isOpeningGate = false
      }
      if !(isSearching == false && isOpeningGate == true) {
        configureForSearchingState(foundGate: !isSearching)
      }
      
    }
  }

  private var isOpeningGate = false {
    didSet {
      if isOpeningGate == true {
        isSearching = false
        openingGateTimer?.invalidate()
        openingGateTimer = Timer.scheduledTimer(timeInterval: Constants.maxGateOpeningTime, target: self,
                                                selector: #selector(gateOpeningTimedOut), userInfo: nil, repeats: false)
      }
      if !(isSearching == true && isOpeningGate == false) {
        configureForOpeningGate(openingGate: isOpeningGate)
      }
    }
  }
  
  weak var delegate: ManualGateOpeningViewControllerDelegate?
  
  // MARK: - Init
  
  init(bluetoothService: BluetoothServiceProtocol, userDefaultsDataStore: UserDefaultsDataStore) {
    self.bluetoothService = bluetoothService
    self.userDefaultsDataStore = userDefaultsDataStore
    super.init(nibName: String(describing: ManualGateOpeningViewController.self), bundle: nil)
    bluetoothService.delegates.add(self)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Status Status Bar
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return UIStatusBarStyle.default
  }
  
  // MARK: - View Controller lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setNeedsStatusBarAppearanceUpdate()
    let foundPeripheral = bluetoothService.hasFoundPeripheralToConnectTo
    isSearching = !foundPeripheral
    configureLogsViews()
    if isOpeningGate {
      configureForOpeningGate(openingGate: isOpeningGate)
    } else if !isSearching {
      configureForSearchingState(foundGate: !isSearching)
    }
    configureLabelsText()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    logsView.stopMonitoring()
  }
  
  // MARK: - Set Label Text
  
  private func configureLabelsText() {
    pushButtonLabel.text = "Нажмите на кнопку для открытия"
    moveCloserToGateLabel.text = "Подойдите поближе"
    noGateDetectedLabel.text = "Устройство не найдено"
    searchingLabel.text = "Поиск устройств поблизости..."
  }
  
  // MARK: - Configure
  
  private func configureForOpeningGate(openingGate: Bool) {
    guard isViewLoaded,
      bluetoothService.centralManagerState != .unsupported
      && bluetoothService.centralManagerState != .poweredOff else {
        return
    }
    
    if openingGate {
      searchingLabel.isHidden = true
      searchingView.isHidden = false
      noGateDetectedView.isHidden = true
      activityIndicator.startAnimating()
      openGatesButton.isEnabled = false
      pushButtonLabel.text = "Идет взаимодействие с устройством"
    } else {
      pushButtonLabel.text = "Нажмите на кнопку, чтобы открыть"
      searchingView.isHidden = true
      activityIndicator.stopAnimating()
      searchingLabel.isHidden = false
      openGatesButton.isEnabled = true
    }
    
  }
  
  private func configureForSearchingState(foundGate: Bool) {

    guard bluetoothService.centralManagerState != .unsupported
      && bluetoothService.centralManagerState != .poweredOff
      && isViewLoaded else {
      return
    }
    pushButtonLabel.text = "Нажмите на кнопку, чтобы открыть"
    
    if foundGate {
      pushButtonLabel.isHidden = false
      openGatesButton.isEnabled = true
      activityIndicator.stopAnimating()
      searchingView.isHidden = true
      noGateDetectedView.isHidden = true
    } else {
      openGatesButton.isEnabled = false
      pushButtonLabel.isHidden = true
      if isSearching {
        searchingLabel.isHidden = false
        searchingView.isHidden = false
        noGateDetectedView.isHidden = false
        activityIndicator.startAnimating()
      }
    }
  }
  
  private func configureLogsViews() {
    logsView.isHidden = true
    view.addSubview(logsView)
    logsView.translatesAutoresizingMaskIntoConstraints = false
    logsView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
    logsView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
    logsView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    logsView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5).isActive = true
  }
  
  // MARK: - Actions
  
  @objc private func gateOpeningTimedOut() {
    isOpeningGate = false
  }
  
  @IBAction private func showLogsButtonPressed(_ sender: Any) {
    let shouldBeHidden = !logsView.isHidden
    logsView.isHidden = shouldBeHidden
    shouldBeHidden ? logsView.stopMonitoring() : logsView.startMonitoring()
    if !shouldBeHidden {
      logsView.scrollToBottom()
    }
    let title = shouldBeHidden ? "Показать логи" : "Спрятать логи"
    showLogsButton.setTitle(title, for: .normal)
  }
  
  @IBAction private func sendButtonPressed(_ sender: Any) {
    guard let logFilePath = Constants.logFilePath else {
      return
    }
    let logFileURL = URL(fileURLWithPath: logFilePath)
    let controller = UIActivityViewController(activityItems: [logFileURL], applicationActivities: nil)
    DispatchQueue.main.async {
      self.present(controller, animated: true, completion: nil)
    }
  }
  
  @IBAction private func manualGateOpenButtonPressed(_ sender: AnyObject) {
    isOpeningGate = true
    bluetoothService.connectAndOpenGate()
  }

}

extension ManualGateOpeningViewController: BluetoothServiceDelegate {
  
  func bluetoothServiceDidDetectBluetoothOff(_ bluetoothService: BluetoothServiceProtocol) {
    isOpeningGate = false
    isSearching = true
  }
  
 func bluetoothServiceDidFailToOpenGate(_ bluetoothService: BluetoothServiceProtocol) {
    isOpeningGate = false
    showAlert(message: "Произошла ошибка")
    isSearching = true
  }
  
  func bluetoothServiceDidLoseGate(_ bluetoothService: BluetoothServiceProtocol) {
    isOpeningGate = false
    isSearching = true
  }
  
  func bluetoothServiceDidOpenGate(_ bluetoothService: BluetoothServiceProtocol) {
    isOpeningGate = false
    isSearching = true
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        readyToManuallyOpenGate gate: String?) {
    isSearching = false
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didFindGate gate: String?) {
    isSearching = false
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol, didRediscoverValidGate gate: String?) {
    isSearching = false
  }
  
  func bluetoothServiceWillOpenGate(_ bluetoothService: BluetoothServiceProtocol) {
    isSearching = false
    isOpeningGate = true
  }
}
