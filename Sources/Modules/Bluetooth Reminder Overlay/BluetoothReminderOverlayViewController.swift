//
//  BluetoothReminderOverlayViewController.swift
//  BLETest
//
//

import UIKit
import BluetoothService

protocol BluetoothReminderOverlayViewControllerDelegate: class {
  func bluetoothReminderOverlayViewControllerFinished(_ bluetoothReminderOverlayVC: BluetoothReminderOverlayViewController)
}

class BluetoothReminderOverlayViewController: BaseViewController {

  // MARK: - Properties
  
  @IBOutlet private var titleLabel: UILabel!
  @IBOutlet private var subtitleLabel: UILabel!
  private let bluetoothService: BluetoothServiceProtocol
  weak var delegate: BluetoothReminderOverlayViewControllerDelegate?
  
  // MARK: - Init
  
  required init(bluetoothService: BluetoothServiceProtocol) {
    self.bluetoothService = bluetoothService
    super.init(nibName: String(describing: BluetoothReminderOverlayViewController.self), bundle: nil)
    modalPresentationCapturesStatusBarAppearance = true
    bluetoothService.delegates.add(self)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - Status Status Bar
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    return UIStatusBarStyle.default
  }
  
  // MARK: - View Controller lifecycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setNeedsStatusBarAppearanceUpdate()
  }
  
  // MARK: - Actions
  
  func finished() {
    delegate?.bluetoothReminderOverlayViewControllerFinished(self)
  }
}

extension BluetoothReminderOverlayViewController: BluetoothServiceDelegate {
  func bluetoothServiceDidDetectBluetoothOn(_ bluetoothService: BluetoothServiceProtocol) {
    finished()
  }
}
