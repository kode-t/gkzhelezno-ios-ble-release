//
//  BluetoothReminderOverlayCoordinator.swift
//  BLETest
//
//

import UIKit
import BluetoothService

class BluetoothReminderOverlayCoordinator: BaseCoordinator {
  
  // MARK: - Properties
  var childCoordinators: [BaseCoordinator] = []
  weak var baseDelegate: BaseCoordinatorDelegate?
  
  let navigationController: UINavigationController
  let bluetoothService: BluetoothServiceProtocol
  
  // MARK: - Init
  
  required init(navigationController: UINavigationController, bluetoothService: BluetoothServiceProtocol) {
    self.navigationController = navigationController
    self.bluetoothService = bluetoothService
  }
  
  // MARK: - Methods
  
  func start() {
    showBluetoothReminderOverlay()
  }
  
  func showBluetoothReminderOverlay() {
    let viewController = BluetoothReminderOverlayViewController(bluetoothService: bluetoothService)
    viewController.delegate = self
    navigationController.definesPresentationContext = true
    viewController.modalPresentationStyle = .overCurrentContext
    navigationController.present(viewController, animated: true, completion: nil)
  }
  
}

extension BluetoothReminderOverlayCoordinator: BluetoothReminderOverlayViewControllerDelegate {
  
  func bluetoothReminderOverlayViewControllerFinished(_ bluetoothReminderOverlayVC: BluetoothReminderOverlayViewController) {
    navigationController.dismiss(animated: true, completion: nil)
    baseDelegate?.coordinatorRootViewControllerDidDeinit(self)
  }
  
}
