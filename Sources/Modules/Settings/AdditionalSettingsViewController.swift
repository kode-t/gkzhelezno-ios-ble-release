//
//  AdditionalSettingsViewController.swift
//  BLETest
//
//

import UIKit
import BluetoothService

class AdditionalSettingsViewController: BaseViewController {
  @IBOutlet private var signalThresholdView: UIView!
  @IBOutlet private var signalPowerView: UIView!
  @IBOutlet private var timeoutView: UIView!
  @IBOutlet private var accessCodeView: UIView!
  @IBOutlet private var protocolView: UIView!
  @IBOutlet private var fieldTestingView: UIView!
  @IBOutlet private var firmwareView: UIView!

  @IBOutlet private var firmwareVersionLabel: UILabel!
  @IBOutlet private var thresholdSlider: UISlider!
  @IBOutlet private var curThresholdValueLabel: UILabel!
  @IBOutlet private var minThresholdValueLabel: UILabel!
  @IBOutlet private var maxThresholdValueLabel: UILabel!
  @IBOutlet private var signalPowerSlider: SteppedSlider!
  @IBOutlet private var curSignalPowerValueLabel: UILabel!
  @IBOutlet private var advertisingTimeoutSlider: SteppedSlider!
  @IBOutlet private var curAdvertisingTimeoutalueLabel: UILabel!
  @IBOutlet private var accessCodeTextField: UITextField!
  @IBOutlet private var wigandProtocolSegmentedControl: UISegmentedControl!
  @IBOutlet private weak var fieldTestingSwitch: UISwitch! {
    didSet {
      fieldTestingSwitch.tintColor = Constants.appColor
      fieldTestingSwitch.onTintColor = Constants.appColor
    }
  }
  
  private var signalPowers = ["-40", "-20", "-16", "-12", "-8", "-4", "0", "+3", "+4"]
  
  private var bluetoothService: BluetoothServiceProtocol
  private let userDefaultsDataStore: UserDefaultsDataStore
  
  // MARK: - Init
  required init(bluetoothService: BluetoothServiceProtocol, userDefaultsDataStore: UserDefaultsDataStore) {
    self.bluetoothService = bluetoothService
    self.userDefaultsDataStore = userDefaultsDataStore
    super.init(nibName: String(describing: AdditionalSettingsViewController.self), bundle: nil)
    bluetoothService.delegates.add(self)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  // MARK: - View Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureLabels()
    configureThresholdSlider()
    configureFieldTestingViews()
    configureSegmentedControl()
    configurePowerSlider()
    configureTimeoutSlider()
    accessCodeTextField.delegate = self
    accessCodeTextField.text = "\(userDefaultsDataStore.accessCode)"
    let tap = UITapGestureRecognizer(target: self, action: #selector(viewTapped(_:)))
    view.addGestureRecognizer(tap)
    hideAdditionalSettings()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    navigationController?.interactivePopGestureRecognizer?.isEnabled = false
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    navigationController?.interactivePopGestureRecognizer?.isEnabled = true
  }
  
  private func configureLabels() {
    minThresholdValueLabel.text = "\(Constants.minRSSIThreshold) dBm"
    maxThresholdValueLabel.text = "\(Constants.maxRSSIThreshold) dBm"
    curThresholdValueLabel.text = "\(Int(bluetoothService.rssiThreshold)) dBm"
    curSignalPowerValueLabel.text = "\(signalPowers.element(at: bluetoothService.signalPowerIndex - 1) ?? "0") dBm"
    if let timeout = bluetoothService.advertisingTimeout {
      curAdvertisingTimeoutalueLabel.text = "\(timeout) с"
    } else {
      curAdvertisingTimeoutalueLabel.text = "не установлен"
    }
    firmwareVersionLabel.text = bluetoothService.firmwareVersion ?? "неизвестно"
  }
  
  private func configureThresholdSlider() {
    let curValue = 1 - ((Constants.maxRSSIThreshold - bluetoothService.rssiThreshold) /
      (Constants.maxRSSIThreshold - Constants.minRSSIThreshold))
    thresholdSlider.setValue(Float(curValue), animated: false)
  }
  
  private func configurePowerSlider() {
    signalPowerSlider.setup(values: Array(0...8).map { Float($0) }, onValueChanged: { [weak self] value in
      guard let self = self, Int(value) < self.signalPowers.count else { return }
      let roundedValue = Int(round(value))
      self.curSignalPowerValueLabel.text = "\(self.signalPowers.element(at: roundedValue) ?? "0") dBm"
    }, onStoppedDragging: { [weak self] value in
        guard let self = self else { return }
      let newSignalPowerIndex = Int(round(value)) + 1
      if self.userDefaultsDataStore.signalPowerIndex != newSignalPowerIndex {
        self.userDefaultsDataStore.signalPowerChanged = true
        self.bluetoothService.shouldChangeSignalPower = true
      }
      self.bluetoothService.signalPowerIndex = newSignalPowerIndex
      self.userDefaultsDataStore.signalPowerIndex = newSignalPowerIndex
    })
    signalPowerSlider.setValue(Float(bluetoothService.signalPowerIndex - 1), animated: false)
  }
  
  private func configureTimeoutSlider() {
    advertisingTimeoutSlider.setup(values: Array(1...15).map { Float($0) }, onValueChanged: { [weak self] value in
      guard let self = self else { return }
      let roundedValue = Int(round(value))
      self.curAdvertisingTimeoutalueLabel.text = "\(roundedValue) с"
    }, onStoppedDragging: { [weak self] value in
      guard let self = self else { return }
      let roundedValue = Int(round(value))
      self.curAdvertisingTimeoutalueLabel.text = "\(roundedValue) с"
      if self.userDefaultsDataStore.advertisingTimeout != roundedValue {
        self.userDefaultsDataStore.advertisingTimeoutChanged = true
        self.bluetoothService.shouldChangeAdvertisingTimeout = true
      }
      self.bluetoothService.advertisingTimeout = roundedValue
      self.userDefaultsDataStore.advertisingTimeout = roundedValue
    })
    advertisingTimeoutSlider.setValue(Float(bluetoothService.advertisingTimeout ?? 0), animated: false)
  }
  
  private func configureFieldTestingViews() {
    fieldTestingSwitch.isOn = userDefaultsDataStore.fieldTesting
  }
  
  private func configureSegmentedControl() {
    var selectedIndex = 0
    switch bluetoothService.wiegandProtocolType {
    case .twentySix:
      selectedIndex = 0
    case .thirtyFour:
      selectedIndex = 1
    case .fourtyTwo:
      selectedIndex = 2
    }
    wigandProtocolSegmentedControl.selectedSegmentIndex = selectedIndex
  }

  private func hideAdditionalSettings() {
    signalThresholdView.isHidden = true
    signalPowerView.isHidden = true
    timeoutView.isHidden = true
    protocolView.isHidden = true
    fieldTestingView.isHidden = true
    firmwareView.isHidden = true
  }
  
  @objc private func viewTapped(_ sender: Any) {
    if let text = accessCodeTextField.text, let accessCode = UInt32(text) {
      userDefaultsDataStore.accessCode = accessCode
      bluetoothService.accessCode = accessCode
    }
    accessCodeTextField.resignFirstResponder()
  }

  @IBAction private func thresholdSliderStoppedDragging(_ sender: AnyObject) {
    var newMinimumRSSIConnectionThreshold = Constants.maxRSSIThreshold - (Decibel(1 - thresholdSlider.value) *
      (Constants.maxRSSIThreshold - Constants.minRSSIThreshold))
    newMinimumRSSIConnectionThreshold = round(newMinimumRSSIConnectionThreshold)
    userDefaultsDataStore.rssiThreshold = newMinimumRSSIConnectionThreshold
    bluetoothService.rssiThreshold = newMinimumRSSIConnectionThreshold
    configureLabels()
  }
  
  @IBAction private func thresholdSliderValueChanged(_ sender: AnyObject) {
    var newMinimumRSSIConnectionThreshold = Constants.maxRSSIThreshold - (Decibel(1 - thresholdSlider.value) *
      (Constants.maxRSSIThreshold - Constants.minRSSIThreshold))
    newMinimumRSSIConnectionThreshold = round(newMinimumRSSIConnectionThreshold)
    curThresholdValueLabel.text = String(describing: Int(newMinimumRSSIConnectionThreshold)) + " dBm"
  }
  
  @IBAction private func wigandProtocolSegmentedControlValueChanged(_ sender: UISegmentedControl) {
    var newProtocol: WiegandProtocolType = .twentySix
    switch sender.selectedSegmentIndex {
    case 0:
      newProtocol = .twentySix
    case 1:
      newProtocol = .thirtyFour
    case 2:
      newProtocol = .fourtyTwo
    default:
      break
    }
    
    userDefaultsDataStore.wigandProtocol = newProtocol
    bluetoothService.wiegandProtocolType = newProtocol
  }
  
  @IBAction private func fieldTestingValueChanged(_ sender: Any) {
    userDefaultsDataStore.fieldTesting = fieldTestingSwitch.isOn
  }
}

extension AdditionalSettingsViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    if let text = textField.text, let accessCode = UInt32(text) {
      userDefaultsDataStore.accessCode = accessCode
      bluetoothService.accessCode = accessCode
    }
    return false
  }
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    let allowedCharacters = CharacterSet(charactersIn: "0123456789")
    let characterSet = CharacterSet(charactersIn: string)
    return allowedCharacters.isSuperset(of: characterSet)
  }
}

extension AdditionalSettingsViewController: BluetoothServiceDelegate {
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol, didReadFirmwareVersion firmwareVersion: String) {
    firmwareVersionLabel.text = firmwareVersion
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol, didReadSignalPower signalPowerIndex: Int) {
    guard !bluetoothService.shouldChangeSignalPower else { return }
    
    if signalPowers.isValidIndex(signalPowerIndex - 1) {
      signalPowerSlider.setValue(Float(signalPowerIndex - 1), animated: false)
      curSignalPowerValueLabel.text = "\(signalPowers.element(at: signalPowerIndex - 1) ?? "0") dBm"
    }
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol, didReadTimeoutInterval timeout: Int) {
    guard !bluetoothService.shouldChangeAdvertisingTimeout else { return }
    curAdvertisingTimeoutalueLabel.text = "\(timeout) с"
    advertisingTimeoutSlider.setValue(Float(timeout), animated: false)
  }
}
