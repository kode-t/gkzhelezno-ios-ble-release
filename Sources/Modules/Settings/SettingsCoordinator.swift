//
//  SettingsCoordinator.swift
//  BLETest
//
//

import UIKit
import BluetoothService

class SettingsCoordinator: BaseCoordinator {
  
  // MARK: - Properties
  
  var childCoordinators: [BaseCoordinator] = []
  weak var baseDelegate: BaseCoordinatorDelegate?

  private var navigationController: UINavigationController
  private let bluetoothService: BluetoothServiceProtocol
  private let userDefaultsDataStore: UserDefaultsDataStore
  private let bluetoothServiceEventHandler: BluetoothServiceEventHandler
  
  // MARK: - Init
  
  required init(navigationController: UINavigationController,
                bluetoothService: BluetoothServiceProtocol,
                bluetoothServiceEventHandler: BluetoothServiceEventHandler,
                userDefaultsDataStore: UserDefaultsDataStore) {
    self.navigationController = navigationController
    self.bluetoothService = bluetoothService
    self.bluetoothServiceEventHandler = bluetoothServiceEventHandler
    self.userDefaultsDataStore = userDefaultsDataStore
  }
  
  // MARK: - Methods
  
  func start() {
    showSettingsScreen()
  }
  
  func showSettingsScreen() {
    let viewModel = SettingsViewModel(userDefaultsDataStore: userDefaultsDataStore,
                                      bluetoothService: bluetoothService,
                                      bluetoothServiceEventHandler: bluetoothServiceEventHandler)
    viewModel.delegate = self
    let viewController = SettingsViewController(viewModel: viewModel)
    viewController.title = "Настройки"
    viewController.delegate = self
    navigationController.pushViewController(viewController, animated: true)
  }
  
  func showAdditionalSettingsScreen() {
    let viewController = AdditionalSettingsViewController(bluetoothService: bluetoothService,
                                                          userDefaultsDataStore: userDefaultsDataStore)
    viewController.title = "Дополнительные настройки"
    navigationController.pushViewController(viewController, animated: true)
  }
}

// MARK: - SettingsViewControllerDelegate

extension SettingsCoordinator: SettingsViewModelDelegate {
  func settingsViewModelDidTapAdditionalSettings(_ viewModel: SettingsViewModel) {
    showAdditionalSettingsScreen()
  }
}

// MARK: - SettingsViewControllerDelegate

extension SettingsCoordinator: SettingsViewControllerDelegate {
  func settingsViewControllerDidDeinit() {
    baseDelegate?.coordinatorRootViewControllerDidDeinit(self)
  }
}
