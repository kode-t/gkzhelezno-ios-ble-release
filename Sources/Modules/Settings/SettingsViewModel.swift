//
//  SettingsViewModel.swift
//  BLETest
//

import Foundation
import BluetoothService

protocol SettingsViewModelProtocol: class {
  var automaticGateOpeningState: Bool { get }

  func onAdditionalSettingsTapped()

  func switchAutomaticGateOpeningState()
}

protocol SettingsViewModelDelegate: class {
  func settingsViewModelDidTapAdditionalSettings(_ viewModel: SettingsViewModel)
}

class SettingsViewModel: SettingsViewModelProtocol {
  
  // MARK: - Properties
  
  private var bluetoothService: BluetoothServiceProtocol
  private let userDefaultsDataStore: UserDefaultsDataStore
  private let bluetoothServiceEventHandler: BluetoothServiceEventHandler
  
  weak var delegate: SettingsViewModelDelegate?
  
  var automaticGateOpeningState: Bool {
    return userDefaultsDataStore.automaticallyOpenGate
  }
  
  // MARK: - Init
  
  init(userDefaultsDataStore: UserDefaultsDataStore,
       bluetoothService: BluetoothServiceProtocol,
       bluetoothServiceEventHandler: BluetoothServiceEventHandler) {
    self.bluetoothService = bluetoothService
    self.userDefaultsDataStore = userDefaultsDataStore
    self.bluetoothServiceEventHandler = bluetoothServiceEventHandler
  }

  // MARK: - Methods
  
  func switchAutomaticGateOpeningState() {
    let openGatesAutomatically = !userDefaultsDataStore.automaticallyOpenGate
    userDefaultsDataStore.automaticallyOpenGate = openGatesAutomatically
    bluetoothService.automaticallyOpenGate = openGatesAutomatically
    bluetoothServiceEventHandler.automaticallyOpenGate = openGatesAutomatically

    if openGatesAutomatically {
      bluetoothService.connectAndOpenGate()
    } else {
      bluetoothService.disconnectFromConnectedPeripheral()
    }
  }
  
  func onAdditionalSettingsTapped() {
    delegate?.settingsViewModelDidTapAdditionalSettings(self)
  }
}
