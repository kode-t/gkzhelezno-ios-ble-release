//
//  SettingsViewController.swift
//  BLETest
//

import UIKit

protocol SettingsViewControllerDelegate: class {
  func settingsViewControllerDidDeinit()
}

class SettingsViewController: BaseViewController {

  // MARK: - Outlets
  
  @IBOutlet private var automaticGateOpeningSwitch: UISwitch!
  @IBOutlet private var additionalSettingsView: UIView!
  
  // MARK: - Properties
  
  private let viewModel: SettingsViewModelProtocol
  weak var delegate: SettingsViewControllerDelegate?
  
  // MARK: - Init
  
  required init(viewModel: SettingsViewModelProtocol) {
    self.viewModel = viewModel
    super.init(nibName: String(describing: SettingsViewController.self), bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  deinit {
    delegate?.settingsViewControllerDidDeinit()
  }
  
  // MARK: - View Life Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    view.tintColor = Constants.appColor
    removeBackButtonTitle()
    let tap = UITapGestureRecognizer(target: self, action: #selector(didTapAdditionalSettings))
    additionalSettingsView.addGestureRecognizer(tap)
    updateAutomaticGateOpeningStateSwitch(with: viewModel.automaticGateOpeningState)
  }
  
  // MARK: - Private Methods

  private func updateAutomaticGateOpeningStateSwitch(with state: Bool) {
    automaticGateOpeningSwitch.setOn(state, animated: true)
  }
  
  // MARK: - Actions
  
  @objc private func didTapAdditionalSettings() {
    viewModel.onAdditionalSettingsTapped()
  }
  
  @IBAction private func switchAutomaticGateOpeningState(_ sender: Any) {
    viewModel.switchAutomaticGateOpeningState()
  }
}
