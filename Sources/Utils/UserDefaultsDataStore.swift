//
//  UserDefaultsDataStore.swift
//  BLETest
//
//

import Foundation
import BluetoothService

class UserDefaultsDataStore {

  struct NSUserDefaultsKeys {
    static let rssiThreshold = "rssiThreshold"
    static let signalPower = "signalPower"
    static let accessCode = "accessCode"
    static let automaticallyOpenGate = "automaticallyOpenGate"
    static let fieldTesting = "isFieldTesting"
    static let wiegandProtocol = "wiegandProtocol"
    static let signalPowerChanged = "signalPowerChanged"
    static let advertisingTimeoutChanged = "advertisingTimeoutChanged"
    static let advertisingTimeout = "advertisingTimeout"
  }
  
  var rssiThreshold: Decibel {
    get {
      var rssiThreshold: Decibel = BluetoothConstants.defaultRSSIThreshold
      let newRSSIThreshold = UserDefaults.standard.double(forKey: NSUserDefaultsKeys.rssiThreshold)
      if newRSSIThreshold != 0 {
        rssiThreshold = newRSSIThreshold
      }
      return min(max(rssiThreshold, Constants.minRSSIThreshold), Constants.maxRSSIThreshold)
    }
    set {
      UserDefaults.standard.set(newValue, forKey: NSUserDefaultsKeys.rssiThreshold)
    }
  }
  
  var accessCode: UInt32 {
    get {
      return UInt32(UserDefaults.standard.integer(forKey: NSUserDefaultsKeys.accessCode))
    }
    set {
      UserDefaults.standard.set(newValue, forKey: NSUserDefaultsKeys.accessCode)
    }
  }
  
  var signalPowerIndex: Int {
    get {
      if UserDefaults.standard.object(forKey: NSUserDefaultsKeys.signalPower) == nil {
        return BluetoothConstants.defaultSignalPowerIndex // default value
      }
      return UserDefaults.standard.integer(forKey: NSUserDefaultsKeys.signalPower)
    }
    set {
      UserDefaults.standard.set(newValue, forKey: NSUserDefaultsKeys.signalPower)
    }
  }
  
  var advertisingTimeout: Int? {
    get {
      if UserDefaults.standard.object(forKey: NSUserDefaultsKeys.advertisingTimeout) == nil {
        return nil
      }
      return UserDefaults.standard.integer(forKey: NSUserDefaultsKeys.advertisingTimeout)
    }
    set {
      UserDefaults.standard.set(newValue, forKey: NSUserDefaultsKeys.advertisingTimeout)
    }
  }
  
  var wigandProtocol: WiegandProtocolType {
    get {
      let rawValue = UserDefaults.standard.integer(forKey: NSUserDefaultsKeys.wiegandProtocol)
      guard rawValue > 0, let type = WiegandProtocolType(rawValue: rawValue) else {
        return BluetoothConstants.defaultWiegandProtocolType
      }
      return type
    }
    set {
      UserDefaults.standard.set(newValue.rawValue, forKey: NSUserDefaultsKeys.wiegandProtocol)
    }
  }
  
  var signalPowerChanged: Bool {
    get {
      return UserDefaults.standard.bool(forKey: NSUserDefaultsKeys.signalPowerChanged)
    }
    set {
      UserDefaults.standard.set(newValue, forKey: NSUserDefaultsKeys.signalPowerChanged)
    }
  }
  
  var advertisingTimeoutChanged: Bool {
    get {
      return UserDefaults.standard.bool(forKey: NSUserDefaultsKeys.advertisingTimeoutChanged)
    }
    set {
      UserDefaults.standard.set(newValue, forKey: NSUserDefaultsKeys.advertisingTimeoutChanged)
    }
  }
  
  var automaticallyOpenGate: Bool {
    get {
      return UserDefaults.standard.bool(forKey: NSUserDefaultsKeys.automaticallyOpenGate)
    }
    set {
      UserDefaults.standard.set(newValue, forKey: NSUserDefaultsKeys.automaticallyOpenGate)
    }
  }
  
  var fieldTesting: Bool {
    get {
      if UserDefaults.standard.object(forKey: NSUserDefaultsKeys.fieldTesting) == nil {
        return true // default value
      }
      return UserDefaults.standard.bool(forKey: NSUserDefaultsKeys.fieldTesting)
    }
    set {
      UserDefaults.standard.set(newValue, forKey: NSUserDefaultsKeys.fieldTesting)
    }
  }
  
  // MARK: - Clear Data
  
  func clearData() {
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.automaticallyOpenGate)
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.rssiThreshold)
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.fieldTesting)
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.signalPower)
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.accessCode)
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.wiegandProtocol)
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.signalPowerChanged)
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.advertisingTimeout)
    UserDefaults.standard.removeObject(forKey: NSUserDefaultsKeys.advertisingTimeoutChanged)
  }
}
