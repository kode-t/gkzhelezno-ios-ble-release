//
//  FileChangesMonitor.swift
//  BLETest
//

import Foundation

class LogsChangesMonitor {
  
  private var dispatchSource: DispatchSourceFileSystemObject?
  private let filePath: String
  
  var onUpdateLogs: (() -> Void)?
  
  init(filePath: String) {
    self.filePath = filePath
  }
  
  deinit {
    stopMonitoring()
  }
  
  func startMonitoring() {
    let fileHandle = open(filePath, O_RDONLY)
    guard fileHandle != -1 else {
      return
    }
    
    let queue = DispatchQueue.global(qos: .default)
    
    let source = DispatchSource.makeFileSystemObjectSource(fileDescriptor: fileHandle,
                                                           eventMask: [.delete, .write, .extend], queue: queue)
    
    source.setEventHandler { [unowned self, unowned source] in
      switch source.data {
      case DispatchSource.FileSystemEvent.write, DispatchSource.FileSystemEvent.extend:
        self.onUpdateLogs?()
      case DispatchSource.FileSystemEvent.delete:
        self.stopMonitoring()
        self.startMonitoring()
      default:
        break
      }
    }
    
    source.resume()
    dispatchSource = source
  }
  
  func stopMonitoring() {
    dispatchSource?.cancel()
    dispatchSource = nil
  }
}
