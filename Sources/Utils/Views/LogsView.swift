//
//  LogsView.swift
//  BLETest
//

import UIKit

class LogsView: UIView {
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("should not be loaded from nib")
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
    stopMonitoring()
  }
  
  private var isMonitoring = false
  private var pausedMonitoring = false
  private var textView = UITextView()
  private let logsChangesMonitor: LogsChangesMonitor?
  
  override init(frame: CGRect) {
    if let filePath = Constants.logFilePath {
      logsChangesMonitor = LogsChangesMonitor(filePath: filePath)
    } else {
      logsChangesMonitor = nil
    }
    
    super.init(frame: frame)
    configure()
    
    logsChangesMonitor?.onUpdateLogs = { [unowned self] in
      DispatchQueue.main.async {
        self.refreshLogs()
      }
    }
    NotificationCenter.default.addObserver(self, selector: #selector(appDidBecomeActive),
                                           name: UIApplication.didBecomeActiveNotification, object: nil)
    NotificationCenter.default.addObserver(self, selector: #selector(appDidEnterBackground),
                                           name: UIApplication.didEnterBackgroundNotification, object: nil)
  }
  
  @objc private func appDidBecomeActive() {
    if !isMonitoring && pausedMonitoring {
      pausedMonitoring = false
      startMonitoring()
    }
  }
  
  @objc private func appDidEnterBackground() {
    if isMonitoring && !pausedMonitoring {
      pausedMonitoring = true
      stopMonitoring()
    }
  }
  
  func startMonitoring() {
    refreshLogs()
    logsChangesMonitor?.startMonitoring()
    if logsChangesMonitor != nil {
      isMonitoring = true
    }
  }
  
  func scrollToBottom() {
    let bottomRect = CGRect(x: textView.contentSize.width - 1, y: textView.contentSize.height - 1,
                            width: 1, height: 1)
    textView.scrollRectToVisible(bottomRect, animated: false)
  }
  
  func stopMonitoring() {
    isMonitoring = false
    logsChangesMonitor?.stopMonitoring()
  }
  
  private func refreshLogs() {
    guard let filePath = Constants.logFilePath, let logs = try? String(contentsOfFile: filePath) else { return }
    
    textView.isSelectable = true
    textView.isEditable = true
    
    textView.text = logs
    let bottomRect = CGRect(x: textView.contentSize.width - 1, y: textView.contentSize.height - 1,
                            width: 1, height: 1)
    textView.scrollRectToVisible(bottomRect, animated: false)
    
    textView.isSelectable = false
    textView.isEditable = false
  }
  
  private func configure() {
    addSubview(textView)
    textView.translatesAutoresizingMaskIntoConstraints = false
    textView.font = textView.font?.withSize(14)
    textView.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
    textView.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    textView.topAnchor.constraint(equalTo: topAnchor).isActive = true
    textView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
    
    let separator = UIView()
    separator.backgroundColor = UIColor.lightGray.withAlphaComponent(0.6)
    addSubview(separator)
    separator.translatesAutoresizingMaskIntoConstraints = false
    separator.leadingAnchor.constraint(equalTo: leadingAnchor).isActive = true
    separator.trailingAnchor.constraint(equalTo: trailingAnchor).isActive = true
    separator.topAnchor.constraint(equalTo: topAnchor).isActive = true
    separator.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
  }
}
