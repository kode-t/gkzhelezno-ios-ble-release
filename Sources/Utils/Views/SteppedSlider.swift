//
//  SteppedSlider.swift
//  BLETest
//

import UIKit

class SteppedSlider: UISlider {
  private var values: [Float] = []
  private var lastIndex: Int?
  var onValueChanged: ((Float) -> Void)?
  var onStoppedDragging: ((Float) -> Void)? 
  
  func setup(values: [Float],
             onValueChanged: @escaping (_ newValue: Float) -> Void,
             onStoppedDragging: @escaping (_ newValue: Float) -> Void) {
    self.values = values
    self.onValueChanged = onValueChanged
    self.onStoppedDragging = onStoppedDragging
    addTarget(self, action: #selector(handleValueChange(sender:)), for: .valueChanged)
    addTarget(self, action: #selector(handleStoppedDragging(sender:)), for: .touchUpInside)
    addTarget(self, action: #selector(handleStoppedDragging(sender:)), for: .touchUpOutside)
    let steps = values.count - 1
    minimumValue = 0
    maximumValue = Float(steps)
  }
  
  @objc func handleValueChange(sender: UISlider) {
    let newIndex = Int(sender.value + 0.5) // round up to next index
    setValue(Float(newIndex), animated: false) // snap to increments
    var didChange = lastIndex == nil
    if !didChange, let index = lastIndex {
      didChange = newIndex != index
    }
    if didChange {
      let actualValue = self.values[newIndex]
      onValueChanged?(actualValue)
    }
  }
  
  @objc func handleStoppedDragging(sender: UISlider) {
    let actualValue = self.values[Int(value)]
    onStoppedDragging?(actualValue)
  }
}
