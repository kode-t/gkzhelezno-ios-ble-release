//
//  SoundPlayer.swift
//  BLETest
//

import Foundation
import AVFoundation
import UserNotifications
import MediaPlayer
import BluetoothService

private struct SoundInfo {
  let url: URL
  let event: EventType
}

private extension EventType {
  var isDebugEvent: Bool {
    switch self {
    case .invalidRSSILevelUnitDiscovered,
         .validRSSILevelUnitDiscovered, .connectionErrorEncountered,
         .charWriteOrResponseErrorEncountered, .charResponseReceivedSuccessfully:
      return true
    default:
      return false
    }
  }
}

final class SoundPlayer: NSObject, SoundPlaying {
  
  private struct LocalNotificationTypes {
    static let sound = "sound"
  }
  
  private var audioPlayer: AVAudioPlayer?
  private var soundQueue: [SoundInfo] = []
  private let userDefaultsDataStore: UserDefaultsDataStore
  private var shouldResumePlayingSystemMusic = false
  
  var isPlayingSound: Bool {
    return audioPlayer?.isPlaying ?? false
  }
  
  init(userDefaultsDataStore: UserDefaultsDataStore) {
    self.userDefaultsDataStore = userDefaultsDataStore
    super.init()
  }
  
  func soundName(forEvent eventType: EventType, includeExtension: Bool = false) -> String {
    switch eventType {
    case .invalidRSSILevelUnitDiscovered:
      return includeExtension ? "meow.caf" : "meow"
    case .validRSSILevelUnitDiscovered:
      return includeExtension ? "bark.caf" : "bark"
    case .connectionErrorEncountered:
      return includeExtension ? "gunshot.caf" : "gunshot"
    case .charWriteOrResponseErrorEncountered:
      return includeExtension ? "carhorn.caf" : "carhorn"
    case .charResponseReceivedSuccessfully:
      return includeExtension ? "success.caf" : "success"
    case .gateOpeningError:
      return includeExtension ? "FailureSound.wav" : "FailureSound"
    case .connectingToGate:
      return includeExtension ? "ConnectionToGateSound.wav" : "ConnectionToGateSound"
    }
  }
  
  private func soundURL(forEvent eventType: EventType) -> URL? {
    let fileName = soundName(forEvent: eventType)
    if let path = Bundle.main.path(forResource: fileName, ofType: "caf")
      ?? Bundle.main.path(forResource: fileName, ofType: "wav") {
      return URL(fileURLWithPath: path)
    }
    return nil
  }
  
  private func stopSound() {
    audioPlayer?.stop()
    audioPlayer = nil
    if MPMusicPlayerController.systemMusicPlayer.playbackState == .interrupted {
      MPMusicPlayerController.systemMusicPlayer.play()
    }
  }
  
  func playSound(forEvent eventType: EventType) {
    guard !eventType.isDebugEvent || userDefaultsDataStore.fieldTesting else { return }
    
    if let soundURL = soundURL(forEvent: eventType) {
      if audioPlayer?.isPlaying == true {
        if let lastSound = soundQueue.last, lastSound.url == soundURL {
          return
        }
        soundQueue.append(SoundInfo(url: soundURL, event: eventType))
        return
      }
      play(url: soundURL, event: eventType)
    }
  }
  
  private func play(url: URL, event: EventType) {
    if UIApplication.shared.applicationState != .active && event.isDebugEvent && userDefaultsDataStore.fieldTesting {
      sendSoundNotification(forSoundURL: url)
    }
    let player = try? AVAudioPlayer(contentsOf: url)
    player?.delegate = self
    audioPlayer = player
    player?.play()
  }
  
  private func sendSoundNotification(forSoundURL soundURL: URL) {
    let after: TimeInterval = 0.05
    let soundName = soundURL.lastPathComponent
    let content = UNMutableNotificationContent()
    content.body = ""
    content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: soundName))
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: after,
                                                    repeats: false)
    let identifier = LocalNotificationTypes.sound
    let request = UNNotificationRequest(identifier: identifier,
                                        content: content, trigger: trigger)
    let center = UNUserNotificationCenter.current()
    center.add(request)
  }
}

extension SoundPlayer: AVAudioPlayerDelegate {
  func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
    if let soundInfo = soundQueue.first {
      soundQueue.removeFirst()
      play(url: soundInfo.url, event: soundInfo.event)
    } else {
      stopSound()
    }
  }
}
