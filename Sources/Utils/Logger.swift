//
//  Logger.swift
//  BLETest
//

import Foundation
import BluetoothService

/**
 Сервис логирования в файл
 */
class Logger: Logging {
  private static let dateFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd-MM-yyyy HH:mm:ss.SSS"
    return formatter
  }()

  private var fileURL: URL? {
    guard let path = filePath else { return nil }
    return URL(fileURLWithPath: path)
  }
  
  private let filePath: String?

  init(logFilePath: String?) {
    self.filePath = logFilePath
  }
  
  /**
   Удалить лог файл.
   */
  func delete() {
    guard let url = fileURL else {
      print("File does not exist")
      return
    }
    let fileManager = FileManager.default
    do {
      try fileManager.removeItem(at: url)
    } catch let error {
      print(error.localizedDescription)
    }
  }
  
  /**
   Добавить запись в лог файл и вывести в консоль.
   
   - Parameter string: строка для записи в лог файл
   */
  func log(_ string: String) {
    guard let url = fileURL else {
      print("File does not exist")
      return
    }
    print("\(Logger.dateFormatter.string(from: Date())) \(string)")
    let logString = [Logger.dateFormatter.string(from: Date()), string, "\n"].joined(separator: " ")
    if let handle = try? FileHandle(forWritingTo: url), let data = logString.data(using: .utf8) {
      handle.seekToEndOfFile()
      handle.write(data)
      handle.closeFile()
    } else {
      try? logString.data(using: .utf8)?.write(to: url)
    }
  }
}
