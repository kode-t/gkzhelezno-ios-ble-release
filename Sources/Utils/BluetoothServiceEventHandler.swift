//
//  BluetoothServiceEventHandler.swift
//  BLETest
//
//

import UIKit
import UserNotifications
import BluetoothService

// MARK: - Constants

struct LocalNotificationAlertActionIdentifier {
  static let open = "open_identifier"
  static let tryAgain = "try_again_identifier"
}

struct LocalNotificationCategoryIdentifier {
  static let openGate = "open_gate_identifier"
  static let tryOpenGateAgain = "try_open_gate_again"
}

private struct LocalNotificationKeys {
  static let type = "type"
  static let tryAgain = "tryAgain"
  static let error = "error"
}

private struct LocalNotificationTypes {
  static let openGate = "openGate"
  static let error = "error"
  static let text = "text"
  static let disappearing = "disappearing"
  static let gateInteracting = "gateInteracting"
}

private extension Constants {
  static let manualOpenGateNotificationClearTime: TimeInterval = 5
  static let gateFoundSoundCooldownTime: TimeInterval = 10
  static let localNotificationDisappearAfter: TimeInterval = 5.0
}

// MARK: - BluetoothServiceEventHandlerDelegate

protocol BluetoothServiceEventHandlerDelegate: class {
  func bluetoothServiceEventHandlerRequestsOpenGateManually(_ bluetoothServiceEventHandler: BluetoothServiceEventHandler)
}

// MARK: - BluetoothServiceEventHandler

/**
 Класс, обрабатывающий события BluetoothService и отправляющий соответствующие локальные нотификации
 (и проигрывающий звук в режиме тестирования)
 */
final class BluetoothServiceEventHandler: NSObject {
  
  // MARK: - Properties
  
  private let bluetoothService: BluetoothServiceProtocol
  private var beaconService: BeaconServiceProtocol
  private let soundPlayer: SoundPlaying?
  private let loggingService: Logging?
  private var gateFoundTimes: [String: Date] = [:]
  private var willShowDisappearingNotification = false
  private var gateInteractionStartedDate: Date?
  private var clearNotificationTimer: Timer? {
    willSet {
      clearNotificationTimer?.invalidate()
    }
  }
  var automaticallyOpenGate: Bool
  
  weak var delegate: BluetoothServiceEventHandlerDelegate?
  
  // MARK: - Init
  
  /**
   Инициализация сервиса.
   
   - Parameter bluetoothService: сервис взаимодействия с BLE устройством.
   - Parameter beaconService: сервис мониторинга за регионами iBeacon.
   - Parameter automaticallyOpenGate: нужно ли открывать устройство автоматически.
   - Parameter loggingService: сервис логирования для дебага.
   - Parameter soundPlayer: проигрыватель звуков для дебага.
   */
  init(bluetoothService: BluetoothServiceProtocol,
       beaconService: BeaconServiceProtocol,
       automaticallyOpenGate: Bool,
       loggingService: Logging? = nil,
       soundPlayer: SoundPlaying? = nil) {
    self.bluetoothService = bluetoothService
    self.beaconService = beaconService
    self.soundPlayer = soundPlayer
    self.loggingService = loggingService
    self.automaticallyOpenGate = automaticallyOpenGate
    super.init()
    bluetoothService.delegates.add(self)
    self.beaconService.delegate = self
  }
  
  // MARK: - Handle Local Notification
  
  /**
   Обработать взаимодействие пользователя с локальной нотификацией.
   
   - Parameter notification: локальная нотификация, с которой взаимодействовал пользователь.
   */
  func handleLocalUserNotification(_ notification: UNNotification) {
    let userInfo = notification.request.content.userInfo
    guard let type = userInfo[LocalNotificationKeys.type] as? String else { return }
    switch type {
    case LocalNotificationTypes.error:
      let tryAgain = (userInfo[LocalNotificationKeys.tryAgain] as? NSNumber)?.boolValue ?? false
      if tryAgain {
        delegate?.bluetoothServiceEventHandlerRequestsOpenGateManually(self)
      }
    case LocalNotificationTypes.openGate:
      delegate?.bluetoothServiceEventHandlerRequestsOpenGateManually(self)
    default:
      break
    }
  }
  
  // MARK: - Register local notification categories
  
  /**
   Зарегистрировать категории нотификаций.
   */
  func registerNotificationCategories() {
    let openAction = UNNotificationAction(identifier: LocalNotificationAlertActionIdentifier.open,
                                          title: "Открыть", options: [])
    let openCategory = UNNotificationCategory(identifier: LocalNotificationCategoryIdentifier.openGate,
                                              actions: [openAction],
                                              intentIdentifiers: [],
                                              options: [])
    let tryAgainAction = UNNotificationAction(identifier: LocalNotificationAlertActionIdentifier.tryAgain,
                                              title: "Попытаться ещё раз", options: [])
    let tryAgainCategory = UNNotificationCategory(identifier: LocalNotificationCategoryIdentifier.tryOpenGateAgain,
                                                  actions: [tryAgainAction],
                                                  intentIdentifiers: [], options: [])
    let center = UNUserNotificationCenter.current()
    center.setNotificationCategories([openCategory, tryAgainCategory])
  }
  
  // MARK: - Check if user has delivered open gate notifications
  
  /**
   Проверить, находится ли в центре нотификаций пользователя активная нотификация ручного открытия ворот
   
   - Parameter completion: коллбэк, вызываемый при завершении выполнения проверки.
   */
  private func checkIfUserHasDeliveredOpenGateNotification(completion: ((Bool) -> Void)? = nil) {
    UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
      let openGateNotifications = notifications.filter { $0.request.identifier == LocalNotificationTypes.openGate }
      let hasDeliveredNotifications = !openGateNotifications.isEmpty
      self.loggingService?.log("User has delivered open gate notifications: \(hasDeliveredNotifications)")
      completion?(hasDeliveredNotifications)
    }
  }
  
  // MARK: - Local Notifications
  
  /**
   Отправить локальную нотификацию ручного открытия ворот
   */
  private func sendManualOpenGateLocalNotification() {
    DispatchQueue.main.async {
      UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [LocalNotificationTypes.openGate])
    }

    if clearNotificationTimer?.isValid == true {
      clearNotificationTimer?.invalidate()
      clearNotificationTimer = nil
      clearNotificationsFromLockScreen()
    }
    
    loggingService?.log("Will show manual open gate notification")
    
    let content = UNMutableNotificationContent()
    content.body = "Обнаружен BLE замок. Нажмите на нотификацию и выберите опцию \"открыть\""
    content.sound = UNNotificationSound.default
    content.categoryIdentifier = LocalNotificationCategoryIdentifier.openGate
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.2,
                                                    repeats: false)
    let identifier = LocalNotificationTypes.openGate
    content.userInfo = [LocalNotificationKeys.type: LocalNotificationTypes.openGate]
    let request = UNNotificationRequest(identifier: identifier,
                                        content: content, trigger: trigger)
    let center = UNUserNotificationCenter.current()
    center.add(request)
  }
  
  /**
   Отправить локальную нотификацию ручного открытия ворот, если это требуется (если в центре нотификаций
   ещё нет подобной нотификации)
   
   - Parameter gate: имя BLE устройства.
   */
  private func sendManualOpenGateNotificationIfNeeded() {
    guard UIApplication.shared.applicationState == .background else { return }
    checkIfUserHasDeliveredOpenGateNotification { hasOpenGateNotifications in
      guard !hasOpenGateNotifications else { return }
      if let date = self.gateInteractionStartedDate,
        Date().timeIntervalSince(date) < Constants.manualOpenGateNotificationClearTime { return }
      self.sendManualOpenGateLocalNotification()
    }
  }
  
  /**
   Отправить локальную нотификацию для ошибки
   
   - Parameter text: текст нотификации.
   - Parameter identifier: идентификатор нотификации.
   - Parameter after: интервал, через который следует запустить нотификацию.
   - Parameter disappearAfter: интервал, через который следует очистить соответствующие типы нотификаций.
   - Parameter isSilent: флаг, показывающий, нужно ли добавлять звук к нотификации.
   - Parameter soundName: имя звука нотификации.
   */
  private func sendLocalNotification(with text: String,
                                     identifier: String = LocalNotificationTypes.text,
                                     after: TimeInterval = 0.5,
                                     disappearAfter: TimeInterval = Constants.localNotificationDisappearAfter,
                                     shouldDisappear: Bool = true,
                                     isSilent: Bool = true,
                                     soundName: String? = nil) {
    
    let content = UNMutableNotificationContent()
    content.body = text
    if !isSilent {
      if let soundName = soundName {
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: soundName))
      } else {
        content.sound = UNNotificationSound.default
      }
    }
    let trigger = UNTimeIntervalNotificationTrigger(timeInterval: after,
                                                    repeats: false)
    let request = UNNotificationRequest(identifier: identifier,
                                        content: content, trigger: trigger)
    let center = UNUserNotificationCenter.current()
    center.add(request)
    
    if shouldDisappear {
      clearNotificationTimer?.invalidate()
      clearNotificationTimer = nil
      
      willShowDisappearingNotification = true
      
      DispatchQueue.main.asyncAfter(deadline: .now() + after) {
        self.clearNotificationTimer = Timer.scheduledTimer(timeInterval: disappearAfter - 0.5,
                                                           target: self,
                                                           selector: #selector(self.clearNotificationsFromLockScreen),
                                                           userInfo: nil,
                                                           repeats: false)
      }
    }
  }
  
  /**
   Отправить локальную нотификацию для ошибки
   
   - Parameter text: текст нотификации.
   - Parameter includeTryAgainButton: добавить кнопку "Попытаться ещё раз".
   - Parameter soundName: имя звука нотификации
   */
  private func sendLocalErrorNotification(with text: String, includeTryAgainButton: Bool, soundName: String? = nil) {
    let sendLocalNotificaitonClosure = {
      let content = UNMutableNotificationContent()
      content.body = text
      
      if let soundName = soundName {
        content.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: soundName))
      } else {
        content.sound = UNNotificationSound.default
      }
      
      if includeTryAgainButton {
        content.categoryIdentifier = LocalNotificationCategoryIdentifier.tryOpenGateAgain
      }
      let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 0.2,
                                                      repeats: false)
      let identifier = LocalNotificationTypes.error
      content.userInfo = [LocalNotificationKeys.type: LocalNotificationTypes.error,
                          LocalNotificationKeys.error: text,
                          LocalNotificationKeys.tryAgain: includeTryAgainButton]
      let request = UNNotificationRequest(identifier: identifier,
                                          content: content, trigger: trigger)
      let center = UNUserNotificationCenter.current()
      center.add(request)
    }
    
    if clearNotificationTimer?.isValid == true || willShowDisappearingNotification {
      DispatchQueue.main.asyncAfter(deadline: .now() + Constants.localNotificationDisappearAfter + 0.5) {
        self.clearNotificationTimer?.invalidate()
        self.clearNotificationTimer = nil
        self.clearNotificationsFromLockScreen()
        sendLocalNotificaitonClosure()
      }
    } else {
      sendLocalNotificaitonClosure()
    }
    
  }
  
/**
 Очистить соответствующие типы нотификаций
 */
  @objc private func clearNotificationsFromLockScreen() {
    clearNotificationTimer?.invalidate()
    clearNotificationTimer = nil
    
    willShowDisappearingNotification = false
    let identifiers = [LocalNotificationTypes.disappearing, LocalNotificationTypes.gateInteracting]
    UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: identifiers)
  } 
}

// MARK: - BluetoothService Delegate

extension BluetoothServiceEventHandler: BluetoothServiceDelegate {
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        readyToManuallyOpenGate gate: String?) {
    if UIApplication.shared.applicationState == .active {
      delegate?.bluetoothServiceEventHandlerRequestsOpenGateManually(self)
    } else {
      sendManualOpenGateNotificationIfNeeded()
    }
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol, didRediscoverValidGate gate: String?) {
    sendManualOpenGateNotificationIfNeeded()
  }
  
  func bluetoothServiceDidLoseGate(_ bluetoothService: BluetoothServiceProtocol) {
    UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [LocalNotificationTypes.openGate])
  }
  
  func bluetoothServiceRSSITooLow(_ bluetoothService: BluetoothServiceProtocol) {
    soundPlayer?.playSound(forEvent: .invalidRSSILevelUnitDiscovered)
    guard automaticallyOpenGate, UIApplication.shared.applicationState != .active else { return }
    sendLocalNotification(with: "Идёт поиск...",
                          identifier: LocalNotificationTypes.disappearing,
                          after: 2.0,
                          disappearAfter: 4.5)
  }
  
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol, didFindGate gate: String?) {
    if let gate = gate {
      if let date = gateFoundTimes[gate] {
        if Date().timeIntervalSince(date) > Constants.gateFoundSoundCooldownTime {
          soundPlayer?.playSound(forEvent: .validRSSILevelUnitDiscovered)
        }
      } else {
        soundPlayer?.playSound(forEvent: .validRSSILevelUnitDiscovered)
      }
      gateFoundTimes[gate] = Date()
    } else {
      soundPlayer?.playSound(forEvent: .validRSSILevelUnitDiscovered)
    }
  }
  
  func bluetoothServiceDidOpenGate(_ bluetoothService: BluetoothServiceProtocol) {
    removeGateConnectingNotification()
    gateFoundTimes = [:]
  }
  
  func bluetoothServiceDidReceiveConnectionError(_ bluetoothService: BluetoothServiceProtocol) {
    removeGateConnectingNotification()
    soundPlayer?.playSound(forEvent: .gateOpeningError)
    if UIApplication.shared.applicationState != .active {
      sendLocalErrorNotification(with: "Ошибка открытия", includeTryAgainButton: false,
                                 soundName: soundPlayer?.soundName(forEvent: .gateOpeningError, includeExtension: true))
    }
  }
  
  func bluetoothServiceDidReceiveReadOrWriteError(_ bluetoothService: BluetoothServiceProtocol) {
    soundPlayer?.playSound(forEvent: .charWriteOrResponseErrorEncountered)
  }

  func bluetoothServiceWillOpenGate(_ bluetoothService: BluetoothServiceProtocol) {
    soundPlayer?.playSound(forEvent: .connectingToGate)
    gateInteractionStartedDate = Date()
    sendLocalNotification(with: "Открытие в процессе...",
                          identifier: LocalNotificationTypes.gateInteracting,
                          after: 0.001,
                          disappearAfter: 2,
                          shouldDisappear: true,
                          isSilent: false,
                          soundName: soundPlayer?.soundName(forEvent: .connectingToGate, includeExtension: true))
  }
  
  func bluetoothServiceDidFailToOpenGate(_ bluetoothService: BluetoothServiceProtocol) {
    removeGateConnectingNotification()
    soundPlayer?.playSound(forEvent: .gateOpeningError)
    if UIApplication.shared.applicationState != .active {
      sendLocalErrorNotification(with: "Произошла ошибка", includeTryAgainButton: true,
                                 soundName: soundPlayer?.soundName(forEvent: .gateOpeningError, includeExtension: true))
    }
  }

  func bluetoothServiceReceivedManualOpenGateCommand(_ bluetoothService: BluetoothServiceProtocol) {
    let identifiers = [LocalNotificationTypes.openGate]
    UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: identifiers)
  }
  
  private func removeGateConnectingNotification() {
    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
      let identifiers = [LocalNotificationTypes.gateInteracting]
      UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: identifiers)
    }
  }
}

// MARK: - BeaconService Delegate

extension BluetoothServiceEventHandler: BeaconServiceDelegate {
  func beaconServiceDidEnterBeaconRegion(_ beaconService: BeaconServiceProtocol) {
    sendLocalNotification(with: "Вход в регион iBeacon",
                          shouldDisappear: true)
  }
  
  func beaconServiceDidExitBeaconRegion(_ beaconService: BeaconServiceProtocol) {
    sendLocalNotification(with: "Выход из региона iBeacon",
                          shouldDisappear: true)
  }
}
