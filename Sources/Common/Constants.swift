//
//  Constants.swift
//  BLETest
//
//

import UIKit
import BluetoothService

struct Constants {
  static let minRSSIThreshold: Decibel = -100
  static let maxRSSIThreshold: Decibel = -30
  
  static let logFileName = "bluetooth_log"
  static let logFileExtension = "txt"
  
  static let appColor = UIColor(red: CGFloat(43) / 255.0,
                                green: CGFloat(152) / 255.0,
                                blue: CGFloat(240) / 255.0,
                                alpha: 1.0)
  
  static var logFilePath: String? {
    guard let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                                  .userDomainMask, true).first else {
                                                                    return nil
    }
    let logFileURL = URL(fileURLWithPath: documentsPath).appendingPathComponent(Constants.logFileName)
                                                        .appendingPathExtension(Constants.logFileExtension)
    return logFileURL.path
  }
}
