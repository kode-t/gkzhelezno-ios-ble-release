//
//  AppDelegate.swift
//  BLETest
//

import UIKit
import UserNotifications
import BluetoothService

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
  
  // MARK: - Properties

  var window: UIWindow?
  private let userDefaultsDataStore = UserDefaultsDataStore()
  private let beaconService: BeaconServiceProtocol = BluetoothServiceFabric.createBeaconService()
  private var mainCoordinator: MainCoordinator?
  private var bluetoothService: BluetoothServiceProtocol?
  private var bluetoothServiceEventHandler: BluetoothServiceEventHandler?
  
  // MARK: - App Life Cycle

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    let center = UNUserNotificationCenter.current()
    center.delegate = self
    initBluetoothService()
    registerForLocalNotifications(application)
    initWindow()
    initMainCoordinator()
    
    bluetoothService?.startMonitoring()
    beaconService.startMonitoring()
    return true
  }
  
  // MARK: - Local Notifications
  
  private func registerForLocalNotifications(_ application: UIApplication) {
    let options: UNAuthorizationOptions = [.alert, .sound]
    let center = UNUserNotificationCenter.current()
    center.requestAuthorization(options: options) { _, _ in
      self.bluetoothServiceEventHandler?.registerNotificationCategories()
    }
  }
  
  // MARK: - Init
  
  private func initWindow() {
    let windowFrame = UIScreen.main.bounds
    window = UIWindow(frame: windowFrame)
    window?.backgroundColor = UIColor.white
    window?.tintColor = Constants.appColor
    window?.makeKeyAndVisible()
  }
  
  private func initMainCoordinator() {
    guard let window = window,
      let bluetoothService = bluetoothService,
      let bluetoothServiceEventHandler = bluetoothServiceEventHandler else {
      fatalError("Window and bluetooth service must be initialised at this stage")
    }
    mainCoordinator = MainCoordinator(window: window,
                                      bluetoothService: bluetoothService,
                                      bluetoothServiceEventHandler: bluetoothServiceEventHandler,
                                      userDefaultsDataStore: userDefaultsDataStore)
    mainCoordinator?.start()
  }
  
  private func initBluetoothService() {
    let loggingService = Logger(logFilePath: Constants.logFilePath)
    let store = userDefaultsDataStore
    let bluetoothServiceConfig = BluetoothServiceConfig(accessCode: store.accessCode,
                                                        rssiThreshold: store.rssiThreshold,
                                                        automaticallyOpenGate: store.automaticallyOpenGate,
                                                        wiegandProtocolType: store.wigandProtocol,
                                                        signalPowerIndex: store.signalPowerIndex,
                                                        advertisingTimeout: store.advertisingTimeout,
                                                        shouldChangeAdvertisingTimeout: store.advertisingTimeoutChanged,
                                                        shouldChangeSignalPower: store.signalPowerChanged)
    bluetoothService = BluetoothServiceFabric.createBluetoothService(bluetoothServiceConfig: bluetoothServiceConfig,
                                                                     canReadDeviceSettings: false,
                                                                     loggingService: loggingService)
    
    let soundPlayer = SoundPlayer(userDefaultsDataStore: store)
    
    guard let bluetoothService = bluetoothService else {
      fatalError("Bluetooth service must be initialised at this stage")
    }
    bluetoothServiceEventHandler = BluetoothServiceEventHandler(bluetoothService: bluetoothService,
                                                                beaconService: beaconService,
                                                                automaticallyOpenGate: store.automaticallyOpenGate,
                                                                loggingService: loggingService,
                                                                soundPlayer: soundPlayer)
  }
}

extension AppDelegate: UNUserNotificationCenterDelegate {
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
                              withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
      self.bluetoothServiceEventHandler?.handleLocalUserNotification(notification)
    }
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    switch response.actionIdentifier {
    case LocalNotificationAlertActionIdentifier.open :
      bluetoothService?.connectAndOpenGate()
    case LocalNotificationAlertActionIdentifier.tryAgain:
      mainCoordinator?.goBackToManualGateOpeningScreen()
    default:
      DispatchQueue.main.async {
        self.bluetoothServiceEventHandler?.handleLocalUserNotification(response.notification)
      }
    }
    completionHandler()
  }
}
