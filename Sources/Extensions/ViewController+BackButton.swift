//
//  ViewController+BackButton.swift
//  BLETest
//
//

import UIKit

extension UIViewController {
  func removeBackButtonTitle() {
    let backBtn = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
    navigationItem.backBarButtonItem = backBtn
  }
}
