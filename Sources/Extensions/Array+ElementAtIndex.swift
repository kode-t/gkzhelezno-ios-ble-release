//
//  Array+ElementAtIndex.swift
//  BLETest
//

import Foundation

extension Array {
  func isValidIndex(_ index: Int) -> Bool {
    return index >= 0 && index < count
  }
  
  func element(at index: Int) -> Element? {
    guard isValidIndex(index) else { return nil }
    return self[index]
  }
}
