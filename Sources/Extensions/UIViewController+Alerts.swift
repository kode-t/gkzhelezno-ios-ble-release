//
//  UIViewController+Alerts.swift
//  BLETest
//

import UIKit

extension UIViewController {
  func showAlert(withMessage message: String,
                 showCancelButton: Bool = false,
                 okButtonTitle: String = "ОК",
                 cancelButtonTitle: String = "Отмена",
                 tapBlock: (() -> Void)? = nil) {
    showAlert(withTitle: "BLE Test", message: message, showCancelButton: showCancelButton,
              okButtonTitle: okButtonTitle, cancelButtonTitle: cancelButtonTitle, tapBlock: tapBlock)
  }
  
  func showAlert(message: String,
                 showCancelButton: Bool = false,
                 okButtonTitle: String = "ОК",
                 cancelButtonTitle: String = "Отмена",
                 tapBlock: (() -> Void)? = nil,
                 cancelTapBlock: (() -> Void)? = nil) {
    showAlert(withTitle: "BLE Test", message: message, showCancelButton: showCancelButton,
              okButtonTitle: okButtonTitle, cancelButtonTitle: cancelButtonTitle, tapBlock: tapBlock,
              cancelTapBlock: cancelTapBlock)
  }
  
  func showAlert(withTitle title: String,
                 message: String,
                 showCancelButton: Bool = false,
                 okButtonTitle: String = "ОК",
                 cancelButtonTitle: String = "Отмена",
                 tapBlock: (() -> Void)? = nil,
                 cancelTapBlock: (() -> Void)? = nil) {
    
    let cancelButtonTitle: String? = showCancelButton ? cancelButtonTitle
      : okButtonTitle
    let otherButtonTitle: String? = showCancelButton ? okButtonTitle : nil
    
    let alertController = UIAlertController(title: title,
                                            message: message,
                                            preferredStyle: .alert)
    
    if let otherButtonTitle = otherButtonTitle {
      alertController.addAction(UIAlertAction(title: cancelButtonTitle, style: .cancel) { _ in
        cancelTapBlock?()
      })
      alertController.addAction(UIAlertAction(title: otherButtonTitle, style: .default) { _ in
        tapBlock?()
      })
    } else {
      alertController.addAction(UIAlertAction(title: cancelButtonTitle, style: .cancel) { _ in
        tapBlock?()
      })
    }
    present(alertController, animated: true, completion: .none)
  }
}
