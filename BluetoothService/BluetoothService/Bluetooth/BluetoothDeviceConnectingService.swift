//
//  BluetoothDeviceConnectingService.swift
//  BLETest
//
//

import UIKit
import CoreBluetooth

protocol BLEConnectingServiceDataSource: class {
  var automaticallyOpenGate: Bool { get }
  var rssiThreshold: Decibel { get }
  func nameForPeripheral(_ peripheral: CBPeripheral) -> String?
  func retrievePeripheralsWithIdentifiers(_ UUIDs: [UUID]) -> [CBPeripheral]
}

protocol BLEConnectingServiceDelegate: class {
  func bleConnectingService(bleConnectingService: BluetoothDeviceConnectingService,
                            requestsDisconnectFromPeripheral peripheral: CBPeripheral)
  func bleConnectingService(bleConnectingService: BluetoothDeviceConnectingService,
                            requestsConnectToPeripheral peripheral: CBPeripheral,
                            shouldOpenGate: Bool,
                            shouldReadInfo: Bool)
  func bleConnectingServiceDidLoseGate(_ bleConnectingService: BluetoothDeviceConnectingService)
  func bleConnectingService(_ bleConnectingService: BluetoothDeviceConnectingService,
                            readyToManuallyOpenGate gate: String?)
  func bleConnectingService(_ bleConnectingService: BluetoothDeviceConnectingService,
                            didFindGate gate: String?)
  func bleConnectingService(_ bleConnectingService: BluetoothDeviceConnectingService,
                            didRediscoverValidGate gate: String?)
  func bleConnectingServicePeripheralRSSITooLow(_ bleConnectingService: BluetoothDeviceConnectingService)
}

/**
 Информация об устройстве, ожидающем команды на открытие от пользователя
 */

private struct WaitingForManualOpenDevice {
  let uuid: UUID
  let dateAdded: Date
  let name: String?
}

/**
  Сервис, определяющий, подключаться к устройству или нет
 */
final class BluetoothDeviceConnectingService: NSObject {
  
  // MARK: - Properties

  weak var delegate: BLEConnectingServiceDelegate?
  weak var dataSource: BLEConnectingServiceDataSource?

  private let canReadDeviceSettings: Bool
  private var deviceLostTimer: Timer?
  private var waitingForManualOpenDevices: [WaitingForManualOpenDevice] = []
  private var loggingService: Logging?
  private var lastGateOpeningDate = Date(timeIntervalSince1970: 0)
  private (set) var hasFoundPeripheralToConnectTo: Bool = false
  private var validDevice: WaitingForManualOpenDevice? {
    return waitingForManualOpenDevices.last
  }
  private var automaticallyOpenGate: Bool {
    return dataSource?.automaticallyOpenGate ?? true
  }
  
  var validPeripheralUUID: UUID? {
    return validDevice?.uuid
  }
  
  var hasReadDeviceData = false
  
  // MARK: - Init
  
  /**
   Инициализация сервиса.

   - Parameter loggingService: сервис логирования для дебага.
   */
  init(canReadDeviceSettings: Bool, loggingService: Logging? = nil) {
    self.loggingService = loggingService
    self.canReadDeviceSettings = canReadDeviceSettings
    super.init()
    NotificationCenter.default.addObserver(self, selector: #selector(handleAppBecameActive),
                                           name: UIApplication.didBecomeActiveNotification,
                                           object: nil)
  }
  
  deinit {
    NotificationCenter.default.removeObserver(self)
  }
  
  // MARK: - Public methods
  
  /**
   Обработать потерю сигнала периферийного устройства, подходящего для подключения.
   */
  @objc func lostPeripheralValidForConnection() {
    clearPeripheralValidForConnection()
    delegate?.bleConnectingServiceDidLoseGate(self)
  }
  
  /**
   Очистить данные о периферийном устройстве, подходящем для подключения.
   */
  func clearPeripheralValidForConnection() {
    loggingService?.log("Peripheral valid for connection no longer considered valid until rediscovery")
    waitingForManualOpenDevices = []
    hasFoundPeripheralToConnectTo = false
  }
  
  /**
   Очистить данные при подключении к устройству.
   
   - Parameter peripheral: периферийное BLE устройство.
   - Parameter connectedForGateOpening: подключение к устройству совершено для отправки команды открытия.
   */
  func clearData(peripheral: CBPeripheral, failedToConnect: Bool = false) {
    deviceLostTimer?.invalidate()
    deviceLostTimer = nil
    
    clearPeripheralValidForConnection()
    if let index = waitingForManualOpenDevices.index(where: { $0.uuid == peripheral.identifier }) {
      waitingForManualOpenDevices.remove(at: index)
    }
    hasFoundPeripheralToConnectTo = false
    
    if failedToConnect {
      delegate?.bleConnectingServiceDidLoseGate(self)
    }
  }
  
  /**
   Обновить дату последней удачной отправки команды открытия на периферийное устройство.
   */
  func updateLastGateOpeningDate(_ date: Date) {
    lastGateOpeningDate = date
  }
  
  /**
   Подключится к устройству, если все необходимые условия выполнены.
   
   - Parameter peripheral: периферийное BLE устройство.
   - Parameter rssi: уровень силы сигнала периферийного устройства.
   */
  func connectToPeripheralIfNeeded(peripheral: CBPeripheral, rssi: Decibel) {
    let readDataIfNeeded = {
      if !self.hasReadDeviceData, self.canReadDeviceSettings {
        self.delegate?.bleConnectingService(bleConnectingService: self,
                                            requestsConnectToPeripheral: peripheral,
                                            shouldOpenGate: false, shouldReadInfo: true)
      }
    }
    
    // если устройство уже было обнаружено в эфире, подключаться для открытия не стоит
    guard checkIfPeripheralShouldBeConsideredForConnecting(peripheral: peripheral) else {
      loggingService?.log("Peripheral is not valid for gate opening attempt")
      readDataIfNeeded()
      return
    }
    
    if rssi < 0 && rssi >= (dataSource?.rssiThreshold ?? -1000) {
      // уровень сигнала выше порогового
      loggingService?.log("RSSI value is valid: \(rssi) for peripheral: \(nameForPeripheral(peripheral) ?? "")")
      handleValidRSSIDeviceDetection(peripheral: peripheral, rssi: rssi)
    } else {
      loggingService?.log("RSSI is not valid: \(rssi) minimum: \(dataSource?.rssiThreshold ?? 0)")
      delegate?.bleConnectingServicePeripheralRSSITooLow(self)
      readDataIfNeeded()
    }
  }
  
  // MARK: - Private methods
  
  /**
   Получить от dataSource имя устройства.
   
   - Parameter peripheral: периферийное BLE устройство.
   */
  private func nameForPeripheral(_ peripheral: CBPeripheral) -> String? {
    return dataSource?.nameForPeripheral(peripheral)
  }
  
  /**
   Запустить таймер, по истечению которого пометить устройство как потерянное в эфире.
   
   - Parameter peripheral: периферийное BLE устройство.
   */
  private func startDeviceLostTimer(for peripheralUUIDString: String) {
    // если устройство не было повторно обнаружено через timeout, либо не было подключено, считать его потерянным в эфире
    let foregroudTimeout = BluetoothConstants.deviceLostInForegroundTimeoutInterval
    let backgroundTimeout = BluetoothConstants.deviceLostInBackgroundTimeoutInterval
    let timeout = UIApplication.shared.applicationState == .active ? foregroudTimeout : backgroundTimeout
    let userInfo = [BluetoothConstants.uuidKey: peripheralUUIDString]
    deviceLostTimer?.invalidate()
    deviceLostTimer = Timer.scheduledTimer(timeInterval: timeout,
                                           target: self,
                                           selector: #selector(handleDeviceLost(timer:)),
                                           userInfo: userInfo,
                                           repeats: false)
  }
  
  /**
   Обработать таймаут повторного обнаружения девайса в эфире.
   
   - Parameter timer: таймер, вызвавший метод.
   */
  @objc private func handleDeviceLost(timer: Timer) {
    guard timer.isValid else { return }
    let userInfo = timer.userInfo as? [String: Any]
    let uuidString = userInfo?[BluetoothConstants.uuidKey] as? String
    handleDeviceLost(uuidString: uuidString)
  }
  
  /**
   Пометить девайс как более не находящийс в эфире.
   
   - Parameter uuidString: идентификатор потерянного устройства.
   */
  private func handleDeviceLost(uuidString: String?) {
    if let uuidString = uuidString,
      let uuid = UUID(uuidString: uuidString),
      let index = waitingForManualOpenDevices.index(where: { $0.uuid == uuid }) {
      waitingForManualOpenDevices.remove(at: index)
    }
    
    hasFoundPeripheralToConnectTo = false
    clearPeripheralValidForConnection()
    delegate?.bleConnectingServiceDidLoseGate(self)
  }
  
  /**
   Проверить, является ли устройство валидным для подключения.
   
   - Parameter peripheral: периферийное BLE устройство.
  
   - Returns: Флаг, показывающий, нужно ли проводить дальнейшие проверки о валидности устроства для подключения
   или нужно игнорировать устройство.
   */
  private func checkIfPeripheralShouldBeConsideredForConnecting(peripheral: CBPeripheral) -> Bool {
    startDeviceLostTimer(for: peripheral.identifier.uuidString)
    
    guard waitingForManualOpenDevices.contains(where: { $0.uuid == peripheral.identifier }) else {
      // устройство ранее не было обнаружено
      return true
    }
    
    loggingService?.log("Peripheral \(nameForPeripheral(peripheral) ?? "") has already been found"
      + " and is waiting for user input. Will not connect")
    
    delegate?.bleConnectingService(self, didRediscoverValidGate: nameForPeripheral(peripheral))
    return false
  }
  
  /**
   Обработать обнаружение устройства с уровнем силы сигнала выше порогового.
   
   - Parameter peripheral: периферийное BLE устройство.
   - Parameter rssi: уровень силы сигнала периферийного устройства.
   */
  private func handleValidRSSIDeviceDetection(peripheral: CBPeripheral, rssi: Double = 0) {
    guard Date().timeIntervalSince(lastGateOpeningDate)
      >= BluetoothConstants.waitingTimeAfterGateOpeningAttempt else {
        loggingService?.log("Not enough time passed since last gate opening attempt")
      // не повторять попытку открытия 5с после последнего открытия
      return
    }
    loggingService?.log("Enough time passed since last gate opening attempt.")
    if automaticallyOpenGate {
      if !hasReadDeviceData, canReadDeviceSettings {
        loggingService?.log("Automatic gate opening is on. Will open gate.")
        deviceLostTimer?.invalidate()
        deviceLostTimer = nil
        delegate?.bleConnectingService(bleConnectingService: self, requestsConnectToPeripheral: peripheral,
                                       shouldOpenGate: true, shouldReadInfo: true)
      } else {
        handleValidRSSIDeviceDetectionAutomatic(peripheral: peripheral)
      }
    } else {
      if !hasReadDeviceData, canReadDeviceSettings {
        delegate?.bleConnectingService(bleConnectingService: self, requestsConnectToPeripheral: peripheral,
                                       shouldOpenGate: false, shouldReadInfo: true)
      }
      handleValidRSSIDeviceDetectionManual(peripheral: peripheral, rssi: rssi)
    }
  }
  
  /**
   Обработать обнаружение устройства с уровнем силы сигнала выше порогового при автоматическом открытии.
   
   - Parameter peripheral: периферийное BLE устройство.
   - Parameter rssi: уровень силы сигнала периферийного устройства.
   */
  private func handleValidRSSIDeviceDetectionAutomatic(peripheral: CBPeripheral) {
    loggingService?.log("Automatic gate opening is on. Will open gate.")
    deviceLostTimer?.invalidate()
    deviceLostTimer = nil
    if let discoveredPeripheral = dataSource?.retrievePeripheralsWithIdentifiers([peripheral.identifier]).first {
      delegate?.bleConnectingService(bleConnectingService: self,
                                     requestsConnectToPeripheral: discoveredPeripheral,
                                     shouldOpenGate: true,
                                     shouldReadInfo: false)
    }
  }
  
  /**
   Обработать обнаружение устройства с уровнем силы сигнала выше порогового при ручном открытии.
   
   - Parameter peripheral: периферийное BLE устройство.
   - Parameter rssi: уровень силы сигнала периферийного устройства.
   */
  private func handleValidRSSIDeviceDetectionManual(peripheral: CBPeripheral, rssi: Double) {
    hasFoundPeripheralToConnectTo = true
    
    let device = WaitingForManualOpenDevice(uuid: peripheral.identifier,
                                            dateAdded: Date(),
                                            name: nameForPeripheral(peripheral))
    
    if let index = waitingForManualOpenDevices.index(where: { $0.uuid == peripheral.identifier }) {
      let waitingDevice = waitingForManualOpenDevices[index]
      if Date().timeIntervalSince(waitingDevice.dateAdded) < BluetoothConstants.waitForManualOpenTime {
        loggingService?.log("Already waiting for user input to open these gates")
        // прошло менее 10 с сообщения о готовности открытия
      } else {
        loggingService?.log("Waiting for user input to open gates")
        waitingForManualOpenDevices.remove(at: index)
        // сообщить о готовности открытия
        delegate?.bleConnectingService(self, readyToManuallyOpenGate: nameForPeripheral(peripheral))
        waitingForManualOpenDevices.append(device)
      }
      delegate?.bleConnectingService(self, didRediscoverValidGate: nameForPeripheral(peripheral))
    } else {
      loggingService?.log("Waiting for user input to open gates")
      delegate?.bleConnectingService(self, didFindGate: nameForPeripheral(peripheral))
      // сообщить о готовности открытия
      delegate?.bleConnectingService(self, readyToManuallyOpenGate: nameForPeripheral(peripheral))
      waitingForManualOpenDevices.append(device)
    }
  }
  
  // MARK: - Notifications
  
  /**
   Обработать возврат приложения в активное состояние.
   */
  @objc private func handleAppBecameActive() {
    guard let date = validDevice?.dateAdded, let identifierString = validPeripheralUUID?.uuidString else { return }
    if Date().timeIntervalSince(date) > BluetoothConstants.deviceLostInForegroundTimeoutInterval {
      handleDeviceLost(uuidString: identifierString)
    } else {
      startDeviceLostTimer(for: identifierString)
    }
  }
}
