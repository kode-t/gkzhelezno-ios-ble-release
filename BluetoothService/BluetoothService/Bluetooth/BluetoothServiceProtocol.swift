//
//  BluetoothServiceProtocol.swift
//  BluetoothService
//

import Foundation
import CoreBluetooth

/**
 Протокол основного сервиса взаимодействия с BLE устройством, осуществляющий поиск устройств. Через этот сервис идет коммуниация
 с остальными блютуз-сервисами
 */
public protocol BluetoothServiceProtocol {
  var wiegandProtocolType: WiegandProtocolType { get set }
  var accessCode: UInt32 { get set }
  var automaticallyOpenGate: Bool { get set }
  var rssiThreshold: Decibel { get set }
  var signalPowerIndex: Int { get set }
  var advertisingTimeout: Int? { get set }
  var firmwareVersion: String? { get }
  var shouldChangeSignalPower: Bool { get set }
  var shouldChangeAdvertisingTimeout: Bool { get set }
  
  var centralManagerState: CBManagerState { get }
  var hasFoundPeripheralToConnectTo: Bool { get }
  var delegates: MulticastDelegate<BluetoothServiceDelegate> { get }
  
  func startMonitoring()
  func stopMonitoring()
  func connectAndOpenGate()
  func disconnectFromConnectedPeripheral()
}
