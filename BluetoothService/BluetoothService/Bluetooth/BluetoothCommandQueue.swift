//
//  BluetoothCommandQueue.swift
//  BluetoothService
//

import Foundation
import CoreBluetooth

/**
Вспомогательная структура для создания очереди команд, подающихся на устройство
*/
struct BluetoothCommandQueue {
  private var commands: [BluetoothCommand] = []
  
  var isEmpty: Bool {
    return commands.isEmpty
  }
  
  var count: Int {
    return commands.count
  }
  
  mutating func enqueue(_ command: BluetoothCommand) {
    commands.append(command)
  }
  
  mutating func dequeue() -> BluetoothCommand? {
    if isEmpty {
      return nil
    } else {
      return commands.removeFirst()
    }
  }
  
  var first: BluetoothCommand? {
    return commands.first
  }
  
  func contains(commandWithType type: BluetoothCommand.CommandType) -> Bool {
    return commands.contains { $0.commandType == type }
  }
}

/**
Вспомогательная структура для обертки отправляемой команды с необходиой задержкой
*/
struct BluetoothCommand {
  enum CommandType: Equatable {
    case setSignalPower(signalPowerIndex: Int), setTimeoutInterval(timeout: Int),
    openGate(wiegand: WiegandProtocolType, accessCode: UInt32), readSignalLevel, readTimeoutInterval, readFirmwareVersion
  }
  
  let commandType: CommandType
  let delay: TimeInterval
  
  init(commandType: CommandType, delay: TimeInterval = 0) {
    self.commandType = commandType
    self.delay = delay
  }
}
