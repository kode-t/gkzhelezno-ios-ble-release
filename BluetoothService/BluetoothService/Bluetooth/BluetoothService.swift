//
//  BluetoothService.swift
//  BLETest
//
//

import UIKit
import CoreBluetooth

/**
 Основной сервис взаимодействия с BLE устройством, осуществляющий поиск устройств. Через этот сервис идет коммуниация
 с остальными блютуз-сервисами
 */
final class BluetoothService: NSObject, BluetoothServiceProtocol {
  private var centralManager: CBCentralManager
  private let bluetoothQueue = DispatchQueue(label: "com.corebluetooth.queue")
  private var loggingService: Logging?
  private let connectingService: BluetoothDeviceConnectingService
  private var interactionService: BluetoothDeviceInteractionService?
  private var disconnectedBecauseOfError = false
  private var peripheralNames: [UUID: String] = [:]
  private var connectingPeripherals: [CBPeripheral] = []
  private var waitingForPeripheralConnectionTimer: Timer?
  private var openGateAfterConnecting = false
  private var readInfoAfterConnecting = false
  
  public var wiegandProtocolType: WiegandProtocolType
  public var accessCode: UInt32
  public var automaticallyOpenGate: Bool
  public var rssiThreshold: Decibel
  public var signalPowerIndex: Int
  public var advertisingTimeout: Int?
  public internal (set) var firmwareVersion: String?
  public var shouldChangeSignalPower: Bool
  public var shouldChangeAdvertisingTimeout: Bool
  public var centralManagerState: CBManagerState {
    return centralManager.state
  }
  public let delegates = MulticastDelegate<BluetoothServiceDelegate>()
  public var hasFoundPeripheralToConnectTo: Bool {
    return connectingService.hasFoundPeripheralToConnectTo
  }
  
  // MARK: - Init
  
  /**
   Инициализация сервиса.
   
   - Parameter bluetoothServiceConfig: структура-обёртка, содержащая необходимые для работы сервиса параметры
   - Parameter loggingService: сервис логирования для дебага.
   */
  init(bluetoothServiceConfig: BluetoothServiceConfig,
       canReadDeviceSettings: Bool = false,
       loggingService: Logging? = nil) {
    self.loggingService = loggingService
    rssiThreshold = bluetoothServiceConfig.rssiThreshold
    automaticallyOpenGate = bluetoothServiceConfig.automaticallyOpenGate
    wiegandProtocolType = bluetoothServiceConfig.wiegandProtocolType
    accessCode = bluetoothServiceConfig.accessCode
    signalPowerIndex = bluetoothServiceConfig.signalPowerIndex
    shouldChangeSignalPower = bluetoothServiceConfig.shouldChangeSignalPower
    advertisingTimeout = bluetoothServiceConfig.advertisingTimeout
    shouldChangeAdvertisingTimeout = bluetoothServiceConfig.shouldChangeAdvertisingTimeout
    centralManager = CBCentralManager(delegate: nil, queue: bluetoothQueue)
    connectingService = BluetoothDeviceConnectingService(canReadDeviceSettings: canReadDeviceSettings,
                                                         loggingService: loggingService)
    super.init()
    
    centralManager.delegate = self
    connectingService.dataSource = self
    connectingService.delegate = self
    startMonitoring()
  }
  
  // MARK: - Peripheral name
  
  /**
   Имя периферийного устройства из вещаемого пакета.
   
   - Parameter peripheral: периферийное BLE устройство.
   */
  func peripheralName(_ peripheral: CBPeripheral) -> String {
    return peripheralNames[peripheral.identifier] ?? ""
  }
  
  // MARK: - Monitor devices
  
  /**
   Инициировать поиск BLE устройств.
   */
  public func startMonitoring() {
    guard centralManager.state == .poweredOn else { return }
    
    let scanOptions = [CBCentralManagerScanOptionAllowDuplicatesKey: true]
    loggingService?.log("Start scanning for peripherals")
    centralManager.scanForPeripherals(withServices: [BluetoothConstants.ServiceUUIDs.advertisedService],
                                      options: scanOptions)
  }
  
  /**
   Остановить поиск BLE устройств.
   */
  public func stopMonitoring() {
    disconnectedBecauseOfError = false
    centralManager.stopScan()
    loggingService?.log("Stop scanning for peripherals")
  }
  
  // MARK: - Open gate
  
  /**
   Соединиться с устройством и инициировать открытие в ручном режиме.
   */
  public func connectAndOpenGate() {
    loggingService?.log("Try to connect to found peripheral and open gate")
    openGateAfterConnecting = true
    tryToConnectToValidPeripheral()
  }
  
  // MARK: - Connect to / disconnect from peripheral

  /**
   Отсоединиться от текущего подсоединенного периферийного устройства.
   */
  public func disconnectFromConnectedPeripheral() {
    if let peripheral = self.interactionService?.peripheral {
      centralManager.cancelPeripheralConnection(peripheral)
    }
    loggingService?.log("Try to disconnect from gate")
  }
  
  /**
   Отсоединиться от периферийного устройства.
   
   - Parameter peripheral: периферийное BLE устройство.
   */
  private func disconnectAndStartMonitoring(peripheral: CBPeripheral) {
    centralManager.cancelPeripheralConnection(peripheral)
    startMonitoring()
    loggingService?.log("Disconnect from connected device and start monitoring again")
  }
  
  /**
   Попытаться подключиться к периферийному устройству с валидными параметрами.
   */
  private func tryToConnectToValidPeripheral() {
    loggingService?.log("Try to connect to valid peripheral")
    
    guard hasFoundPeripheralToConnectTo, let validPeripheralUUID = connectingService.validPeripheralUUID else {
      didLoseGate()
      loggingService?.log("Could not find valid peripheral")
      return
    }
    
    guard let peripheral = centralManager.retrievePeripherals(withIdentifiers: [validPeripheralUUID]).first else { return }
    
    connectingService.clearPeripheralValidForConnection()
    
    if peripheral.state == .connected {
      loggingService?.log("Already connected")
      handleConnected(peripheral: peripheral)
    } else {
      loggingService?.log("Will connect to peripheral")
      connectToPeripheral(peripheral)
    }
  }
  
  /**
   Подсоединиться к периферийному устройству.
   
   - Parameter peripheral: периферийное BLE устройство.
   */
  private func connectToPeripheral(_ peripheral: CBPeripheral) {
    let startConnectionTimeoutClosure = {
      let userInfo: [String: Any] = [BluetoothConstants.peripheralKey: peripheral]
      let timeout = UIApplication.shared.applicationState == .active
        ? BluetoothConstants.connectionTimeoutForeground
        : BluetoothConstants.connectionTimeoutBackground
      let selector = #selector(self.cancelPendingPeripheralConnection)
      self.waitingForPeripheralConnectionTimer?.invalidate()
      self.waitingForPeripheralConnectionTimer = Timer.scheduledTimer(timeInterval: timeout,
                                                                      target: self,
                                                                      selector: selector,
                                                                      userInfo: userInfo,
                                                                      repeats: false)
    }
    
    DispatchQueue.main.async {
      guard peripheral.state != .connecting && peripheral.state != .connected
        && !self.connectingPeripherals.contains(where: { $0 == peripheral }) else {
          self.loggingService?.log("Already connecting to peripheral")
          if self.waitingForPeripheralConnectionTimer == nil {
            startConnectionTimeoutClosure()
          }
          return
      }
      
      self.loggingService?.log("will connect to peripheral: \(self.peripheralName(peripheral))")
      // коннект к BLE не имеет таймаута на уровне фреймворка, поэтому небходимо отключать повисшее соединение вручную
      startConnectionTimeoutClosure()
      if !self.connectingPeripherals.contains(where: { $0 == peripheral }) {
        self.connectingPeripherals.append(peripheral)
      }
      self.centralManager.connect(peripheral, options: [:])
    }
  }
  
  /**
   Обработать неудачную попытку подключения к периферийнму устройству.
   
   - Parameter peripheral: периферийное BLE устройство.
   */
  private func failedToConnectToPeripheral(peripheral: CBPeripheral) {
    if let index = connectingPeripherals.index(where: { $0 == peripheral }) {
      connectingPeripherals.remove(at: index)
    }
    didLoseGate()
    connectingService.clearData(peripheral: peripheral, failedToConnect: true)
    startMonitoring()
    loggingService?.log("Failed to connect to to peripheral: \(peripheralName(peripheral))")
  }
  
  /**
   Отменить попытку подключения к периферийнму устройству.
   
   - Parameter timer: таймер, по которому сработал вызов метода.
   */
  @objc private func cancelPendingPeripheralConnection(timer: Timer) {
    didReceiveConnectionError()
    
    guard let userInfo = timer.userInfo as? [String: Any],
      let peripheral = userInfo[BluetoothConstants.peripheralKey] as? CBPeripheral else { return }
    
    centralManager.cancelPeripheralConnection(peripheral)
    failedToConnectToPeripheral(peripheral: peripheral)
    connectingService.lostPeripheralValidForConnection()
    loggingService?.log("Peripheral connection attempt timed out: \(peripheralName(peripheral))")
    startMonitoring()
  }
  
  /**
   Создать сервис для взаимодействия с девайсом.
   */
  private func createInteractionService(for peripheral: CBPeripheral) -> BluetoothDeviceInteractionService {
    let config = BluetoothDeviceInteractionServiceConfig(wiegandProtocolType: wiegandProtocolType,
                                                         accessCode: accessCode,
                                                         signalPowerIndex: signalPowerIndex,
                                                         timeoutInterval: advertisingTimeout,
                                                         shouldUpdateSignalPower: shouldChangeSignalPower,
                                                         shouldUpdateTimeoutInterval: shouldChangeAdvertisingTimeout)
    return BluetoothDeviceInteractionService(peripheral: peripheral,
                                             config: config,
                                             shouldReadInfo: readInfoAfterConnecting,
                                             shouldOpenGate: openGateAfterConnecting,
                                             loggingService: loggingService)
  }
  
  /**
   Обработать подключение к BLE устройству.
   */
  private func handleConnected(peripheral: CBPeripheral) {
    didFindGate(peripheralName(peripheral))
    interactionService = createInteractionService(for: peripheral)
    interactionService?.delegate = self
    interactionService?.start()
    connectingService.clearData(peripheral: peripheral)
    readInfoAfterConnecting = false
    openGateAfterConnecting = false
    centralManager.stopScan()
  }
}

// MARK: - CBCentralManager Delegate

extension BluetoothService: CBCentralManagerDelegate {
  public func centralManagerDidUpdateState(_ central: CBCentralManager) {
    loggingService?.log("Central manager did update state: \(central.state.rawValue)")
    if central.state == .poweredOn {
      startMonitoring()
    } else if central.state == .poweredOff {
      waitingForPeripheralConnectionTimer?.fire()
      stopMonitoring()
      connectingService.clearPeripheralValidForConnection()
    }
    didChangeBluetoothState(to: central.state)
  }
  
  public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral,
                             advertisementData: [String: Any], rssi RSSI: NSNumber) {
    let localName = advertisementData[CBAdvertisementDataLocalNameKey] as? String
    let name = (localName ?? peripheral.name) ?? "unknown unit"
    peripheralNames[peripheral.identifier] = name
    loggingService?.log("Discovered peripheral: \(name)  rssi value: \(RSSI) advertisemsnt data: \(advertisementData)")
    
    guard peripheral.state != .connected, peripheral.state != .connecting else {
      loggingService?.log("Discovered peripheral is already being connected to, will stop scan")
      stopMonitoring()
      return
    }
    
    DispatchQueue.main.async {
      self.connectingService.connectToPeripheralIfNeeded(peripheral: peripheral, rssi: RSSI.doubleValue)
    }
  }

  public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
    if let index = connectingPeripherals.index(where: { $0 == peripheral }) {
      connectingPeripherals.remove(at: index)
    }
    waitingForPeripheralConnectionTimer?.invalidate()
    waitingForPeripheralConnectionTimer = nil

    loggingService?.log("Connected to valid peripheral: \(peripheralName(peripheral)), will attempt to open gate")
    handleConnected(peripheral: peripheral)
  }
  
  public func centralManager(_ central: CBCentralManager,
                             didFailToConnect peripheral: CBPeripheral, error: Error?) {
    if let index = connectingPeripherals.index(where: { $0 == peripheral }) {
      connectingPeripherals.remove(at: index)
    }
    didReceiveConnectionError()
    
    waitingForPeripheralConnectionTimer?.invalidate()
    waitingForPeripheralConnectionTimer = nil

    loggingService?.log("failed to connect to peripheral \(peripheralName(peripheral)),"
      + " error: \(String(describing: error))")
    
    failedToConnectToPeripheral(peripheral: peripheral)
  }
  
  public func centralManager(_ central: CBCentralManager,
                             didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {

    loggingService?.log("disconnected from peripheral \(peripheralName(peripheral)),"
      + " error: \(String(describing: error))")
    didLoseGate()
    
    if let error = error, (error as NSError).code != BluetoothConstants.deviceInitiatedDisconnectErrorCode {
      disconnectedBecauseOfError = true
    }
    
    guard !disconnectedBecauseOfError else {
      // если отсоединение произошло по ошибке, попробовать подсоединиться к периферийному устройству заново
      disconnectedBecauseOfError = false
      if let discoveredPeripheral = centralManager.retrievePeripherals(withIdentifiers: [peripheral.identifier]).first {
        interactionService?.cleanup()
        interactionService = nil
        connectToPeripheral(discoveredPeripheral)
      }
      return
    }
    
    // очистить данные об отсоединенном устройстве и начать сканировать заново
    if interactionService?.peripheral.identifier == peripheral.identifier {
      interactionService?.cleanup()
      interactionService = nil
    }
    startMonitoring()
  }
}

// MARK: - Connecting service delegate

extension BluetoothService: BLEConnectingServiceDelegate {
  func bleConnectingService(bleConnectingService: BluetoothDeviceConnectingService,
                            requestsConnectToPeripheral peripheral: CBPeripheral, shouldOpenGate: Bool, shouldReadInfo: Bool) {
    openGateAfterConnecting = shouldOpenGate
    readInfoAfterConnecting = shouldReadInfo
    connectToPeripheral(peripheral)
  }
  
  func bleConnectingService(bleConnectingService: BluetoothDeviceConnectingService,
                            requestsDisconnectFromPeripheral peripheral: CBPeripheral) {
    disconnectAndStartMonitoring(peripheral: peripheral)
  }

  func bleConnectingService(_ bleConnectingService: BluetoothDeviceConnectingService,
                            didFindGate gate: String?) {
    loggingService?.log("Did find gate: \(String(describing: gate))")
    didFindGate(gate)
  }
  
  func bleConnectingService(_ bleConnectingService: BluetoothDeviceConnectingService,
                            didRediscoverValidGate gate: String?) {
    loggingService?.log("Did rediscover valid gate: \(String(describing: gate))")
    didRediscoverValidGate(gate)
  }
  
  func bleConnectingServiceDidLoseGate(_ bleConnectingService: BluetoothDeviceConnectingService) {
    loggingService?.log("Did lose gate")
    didLoseGate()
  }
  
  func bleConnectingService(_ bleConnectingService: BluetoothDeviceConnectingService,
                            readyToManuallyOpenGate gate: String?) {
    readyToManuallyOpenGate(gate)
  }
  
  func bleConnectingServicePeripheralRSSITooLow(_ bleConnectingService: BluetoothDeviceConnectingService) {
    rssiTooLow()
  }
}

// MARK: - Connecting service data source

extension BluetoothService: BLEConnectingServiceDataSource {
  func nameForPeripheral(_ peripheral: CBPeripheral) -> String? {
    return peripheralName(peripheral)
  }
  
  func retrievePeripheralsWithIdentifiers(_ uuids: [UUID]) -> [CBPeripheral] {
    return centralManager.retrievePeripherals(withIdentifiers: uuids)
  }
}

// MARK: - DeviceInteractionService Delegate

extension BluetoothService: BluetoothDeviceInteractionServiceDelegate {
  func peripheralInteractionServiceDidReceiveReadOrWriteError(_ bleInteractionService: BluetoothDeviceInteractionService) {
    didReceiveReadOrWriteError()
    didFailToOpenGate()
  }
  
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didReceiveDeviceNameChangedEvent name: String?) {
    peripheralNames[bleInteractionService.peripheral.identifier] = name
  }
  
  func peripheralInteractionServiceWillOpenGate(_ bleInteractionService: BluetoothDeviceInteractionService) {
    loggingService?.log("Will open gate")
    willOpenGate()
  }
  
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didRequestPeripheralDisconnectWithReconnectAttempt shouldReconnect: Bool,
                                    shouldReadData: Bool, shouldOpenGate: Bool) {
    let peripheral = bleInteractionService.peripheral
    readInfoAfterConnecting = shouldReadData
    openGateAfterConnecting = shouldOpenGate
    disconnectedBecauseOfError = shouldReconnect
    loggingService?.log("Requested peripheral disconnect: \(peripheralName(peripheral)) should reconnect: \(shouldReconnect)")
    centralManager.cancelPeripheralConnection(peripheral)
    startMonitoring()
  }
  
  func peripheralInteractionServiceDidOpenGate(_ bleInteractionService: BluetoothDeviceInteractionService) {
    let peripheral = bleInteractionService.peripheral
    centralManager.cancelPeripheralConnection(peripheral)
    connectingService.updateLastGateOpeningDate(Date())
    connectingService.lostPeripheralValidForConnection()
    didOpenGate()
    peripheralNames[bleInteractionService.peripheral.identifier] = nil
    loggingService?.log("Did open gate successfully")
  }
  
  func peripheralInteractionServiceDidChangeSignalPower(_ bleInteractionService: BluetoothDeviceInteractionService) {
    shouldChangeSignalPower = false
    didChangeSignalPower()
    loggingService?.log("Did change signal power successfully")
  }
  
  func peripheralInteractionServiceDidChangeTimeoutInterval(_ bleInteractionService: BluetoothDeviceInteractionService) {
    shouldChangeAdvertisingTimeout = false
    didChangeTimeoutInterval()
    loggingService?.log("Did change timeout interval successfully")
  }
  
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didReadTimeoutInterval timeout: Int) {
    if advertisingTimeout == timeout, shouldChangeAdvertisingTimeout {
      shouldChangeAdvertisingTimeout = false
    } else if !shouldChangeAdvertisingTimeout || advertisingTimeout == nil {
      advertisingTimeout = timeout
    }
    didReadTimeoutInterval(timeout)
    connectingService.hasReadDeviceData = true
  }
  
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didReadSignalPower signalPower: Int) {
    if signalPowerIndex == signalPower, shouldChangeSignalPower {
      shouldChangeSignalPower = false
    } else if !shouldChangeSignalPower {
      signalPowerIndex = signalPower
    }
    didReadSignalPower(signalPower)
    connectingService.hasReadDeviceData = true
  }
  
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didReadFirmwareVersion firmwareVersion: String) {
    self.firmwareVersion = firmwareVersion
    didReadFirmwareVersion(firmwareVersion)
    connectingService.hasReadDeviceData = true
  }
}
