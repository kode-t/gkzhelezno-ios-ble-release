//
//  BluetoothService+Delegate.swift
//  BLETest
//

import Foundation
import CoreBluetooth

// MARK: - MulticastDelegate

/**
 Мультиделегат
 */
public class MulticastDelegate<T> {
  private var delegates: [Weak] = []
  
  /**
   Добавить делегат.
   
   - Parameter delegate: делегат MulticastDelegate.
   */
  public func add(_ delegate: T) {
    delegates.append(Weak(value: delegate as AnyObject))
  }
  
  /**
   Убрать делегат.
   
   - Parameter delegate: делегат MulticastDelegate.
   */
  public func remove(_ delegate: T) {
    let weak = Weak(value: delegate as AnyObject)
    if let index = delegates.index(of: weak) {
      delegates.remove(at: index)
    }
  }
  
  /**
   Вызвать замыкание для всех делегатов.
   
   - Parameter invocation: замыкание, которое нужно вызвать на каждом элементе мультиделегата.
   */
  func invoke(_ invocation: @escaping (T) -> Void) {
    delegates = delegates.filter { $0.value != nil }
    delegates.forEach {
      if let delegate = $0.value as? T {
        invocation(delegate)
      }
    }
  }
}

/**
 Обёртка для хранения слабой ссылки на объект
 */
private struct Weak: Equatable {
  weak var value: AnyObject?
  
  static func == (lhs: Weak, rhs: Weak) -> Bool {
    return lhs.value === rhs.value
  }
}

// MARK: - BluetoothServiceDelegate

/**
 Делегат BluetoothService
 */
public protocol BluetoothServiceDelegate: class {
  func bluetoothServiceDidFailToOpenGate(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceDidOpenGate(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceWillOpenGate(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceDidLoseGate(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceDidReceiveConnectionError(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceRSSITooLow(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceDidReceiveReadOrWriteError(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceDidDetectBluetoothOn(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceDidDetectBluetoothOff(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceDidChangeSignalPower(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothServiceDidChangeTimeoutInterval(_ bluetoothService: BluetoothServiceProtocol)
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didReadTimeoutInterval timeout: Int)
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didReadSignalPower signalPowerIndex: Int)
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didReadFirmwareVersion firmwareVersion: String)
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didFindGate gate: String?)
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        readyToManuallyOpenGate gate: String?)
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didRediscoverValidGate gate: String?)
}

// MARK: - BluetoothServiceDelegate default method implementations

public extension BluetoothServiceDelegate {
  func bluetoothServiceDidFailToOpenGate(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceDidOpenGate(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceWillOpenGate(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceDidLoseGate(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceDidReceiveConnectionError(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceRSSITooLow(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceDidReceiveReadOrWriteError(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceDidDetectBluetoothOn(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceDidDetectBluetoothOff(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceDidChangeSignalPower(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothServiceDidChangeTimeoutInterval(_ bluetoothService: BluetoothServiceProtocol) { }
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didFindGate gate: String?) { }
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        readyToManuallyOpenGate gate: String?) { }
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didRediscoverValidGate gate: String?) { }
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didReadTimeoutInterval timeout: Int) { }
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didReadSignalPower signalPowerIndex: Int) { }
  func bluetoothService(_ bluetoothService: BluetoothServiceProtocol,
                        didReadFirmwareVersion firmwareVersion: String) { }
}

// MARK: - Delegates management

extension BluetoothService {
  /**
   Отправить делегату коллбэк при неудачной попытке открытия.
   */
  func didFailToOpenGate() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceDidFailToOpenGate(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк при успешной попытке открытия.
   */
  func didOpenGate() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceDidOpenGate(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк при инициации попытки открытия.
   */
  func willOpenGate() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceWillOpenGate(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк о потере устройства в эфире.
   */
  func didLoseGate() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceDidLoseGate(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк при неудачной попытке подключения к устройству.
   */
  func didReceiveConnectionError() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceDidReceiveConnectionError(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк при обнаружении уровня сигнала устройства ниже порогового.
   */
  func rssiTooLow() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceRSSITooLow(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк при получении ошибки при записи в характеристику / чтении из характеристики устройства.
   */
  func didReceiveReadOrWriteError() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceDidReceiveReadOrWriteError(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк при удачной установке мощности вещания устройства.
   */
  func didChangeSignalPower() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceDidChangeSignalPower(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк при удачной установке таймаута вещания устройства.
   */
  func didChangeTimeoutInterval() {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothServiceDidChangeTimeoutInterval(self) }
    }
  }
  
  /**
   Отправить делегату коллбэк при изменении состояния Bluetooth на iOS устройстве.
   
   - Parameter state: текущее состояние Bluetooth на iOS устройстве.
   */
  func didChangeBluetoothState(to state: CBManagerState) {
    DispatchQueue.main.async {
      switch state {
      case .poweredOn:
        self.delegates.invoke { $0.bluetoothServiceDidDetectBluetoothOn(self) }
      case .poweredOff:
        self.delegates.invoke { $0.bluetoothServiceDidDetectBluetoothOff(self) }
      default:
        break
      }
    }
  }
  
  /**
   Отправить делегату коллбэк об обнаружении BLE устройства, валидного для попытки открытия.
   
   - Parameter gate: имя BLE устройства.
   */
  func didFindGate(_ gate: String?) {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothService(self, didFindGate: gate) }
    }
  }
  
  /**
   Отправить делегату коллбэк об ожидании команды открытия от пользователя.
   
   - Parameter gate: имя BLE устройства.
   */
  func readyToManuallyOpenGate(_ gate: String?) {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothService(self, readyToManuallyOpenGate: gate) }
    }
  }
  
  /**
   Отправить делегату коллбэк о повторном обнаружении BLE устройства, ожидающего команды открытия от пользователя.
   
   - Parameter gate: имя BLE устройства.
   */
  func didRediscoverValidGate(_ gate: String?) {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothService(self, didRediscoverValidGate: gate) }
    }
  }
  
  /**
   Отправить делегату коллбэк о прочитанном значении таймаута вещания BLE устройства.
   
   - Parameter timeout: таймаут вещания BLE устройства.
   */
  func didReadTimeoutInterval(_ timeout: Int) {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothService(self, didReadTimeoutInterval: timeout) }
    }
  }
  
  /**
   Отправить делегату коллбэк о прочитанном значении силы вещания BLE устройства.
   
   - Parameter signalPowerIndex: индекс силы вещания устройства в массиве возможных значений силы вещания
   устройства. Массив возможных значений вещания:
   [-40 дБм, -20 дБм, -16 дБм, -12 дБм, -8 дБм, -4 дБм, 0 дБм, +3 дБм, +4 дБм].
   */
  func didReadSignalPower(_ signalPowerIndex: Int) {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothService(self, didReadSignalPower: signalPowerIndex) }
    }
  }
  
  /**
   Отправить делегату коллбэк о прочитанном значении прошивки BLE устройства.
   
   - Parameter firmwareVersiob: версия прошивки BLE устройства.
   */
  func didReadFirmwareVersion(_ firmwareVersion: String) {
    DispatchQueue.main.async {
      self.delegates.invoke { $0.bluetoothService(self, didReadFirmwareVersion: firmwareVersion) }
    }
  }
}
