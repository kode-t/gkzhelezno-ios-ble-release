//
//  BluetoothCommandService.swift
//  BLETest
//
//

import Foundation
import CoreBluetooth

/**
   Сервис, отправляющий команды на периферийное BLE устройство.
 */
struct BluetoothCommandService {
  let loggingService: Logging?
  
  init(loggingService: Logging?) {
    self.loggingService = loggingService
  }
  
  /**
   Запускает первую команду из очереди.
   */
  func executeFirstCommand(in queue: BluetoothCommandQueue, on peripheral: CBPeripheral) {
    guard let command = queue.first else { return }
    if command.delay > 0 {
      DispatchQueue.main.asyncAfter(deadline: .now() + command.delay) {
        self.execute(command: command, on: peripheral)
      }
    } else {
      execute(command: command, on: peripheral)
    }
  }
  
  /**
   Отправляет команду установки уровня мощности вещания на BLE устройство.
   
   - Parameter powerLevel:  число от 1 до 9. Число 1 соответствует минимальной мощности -40dBm, 9 - максимальной +4dBm.
   Полный набор значений мощности: -40dBm, -20dBm, -16dBm, -12dBm, -8dBm, -4dBm, 0dBm, +3dBm и +4dBm.
   - Parameter to: периферийное устройство, на которое нужно отправить команду.
   */
  func sendPowerLevelCommand(powerLevel: Int, to peripheral: CBPeripheral) {
    let data = BluetoothCommandData.powerLevelCommand(powerLevel: powerLevel)
    write(data: data, to: peripheral, serviceUUID: BluetoothConstants.ServiceUUIDs.controlService,
          characteristicUUID: BluetoothConstants.ControlServiceCharacteristicUUIDs.command)
    
  }
  
  /**
   Отправляет команду передачи ключа на BLE устройство.
   
   - Parameter wiegand: тип протокола Wiegand.
   - Parameter accessCode: ключ доступа.
   - Parameter to: периферийное устройство, на которое нужно отправить команду
   */
  
  func sendAccesssCodeCommand(wiegand: WiegandProtocolType, accessCode: UInt32, to peripheral: CBPeripheral) {
    let data = BluetoothCommandData.accessCodeCommand(wiegand: wiegand, accessCode: accessCode)
    write(data: data, to: peripheral, serviceUUID: BluetoothConstants.ServiceUUIDs.controlService,
          characteristicUUID: BluetoothConstants.ControlServiceCharacteristicUUIDs.command)
  }
  
  /**
   Отправляет команду чтения текущего уровня мощности сигнала на BLE устройство.
  
   - Parameter to: периферийное устройство, на которое нужно отправить команду
   */
  func sendCurrentPowerLevelCommand(to peripheral: CBPeripheral) {
    let data = BluetoothCommandData.currentPowerLevelCommand()
    write(data: data, to: peripheral, serviceUUID: BluetoothConstants.ServiceUUIDs.controlService,
          characteristicUUID: BluetoothConstants.ControlServiceCharacteristicUUIDs.command)
  }
  
  /**
   Отправляет команду чтения текущей версии прошивки на BLE устройство.
   
   - Parameter to: периферийное устройство, на которое нужно отправить команду
   */
  func sendCurrentFirmwareVersionCommand(to peripheral: CBPeripheral) {
    let data = BluetoothCommandData.currentFirmwareVersionCommand()
    write(data: data, to: peripheral, serviceUUID: BluetoothConstants.ServiceUUIDs.controlService,
          characteristicUUID: BluetoothConstants.ControlServiceCharacteristicUUIDs.command)
  }
  
  /**
   Отправляет команду чтения текущего таймаута вещания на BLE устройство.
   
   - Parameter to: периферийное устройство, на которое нужно отправить команду
   */
  func sendCurrentAdvertisingTimeoutCommand(to peripheral: CBPeripheral) {
    let data = BluetoothCommandData.currentAdvertisingTimeout()
    write(data: data, to: peripheral, serviceUUID: BluetoothConstants.ServiceUUIDs.controlService,
          characteristicUUID: BluetoothConstants.ControlServiceCharacteristicUUIDs.command)
  }
  
  /**
   Отправляет команду установки таймаута вещания на BLE устройство.
   
   - Parameter timeout: таймаут вещания после открытия, в секундах от 0 до 255.
   - Parameter to: периферийное устройство, на которое нужно отправить команду.
   */
  func sendTimeoutIntervalCommand(timeout: Int, to peripheral: CBPeripheral) {
    let data = BluetoothCommandData.advertisingTimeout(timeout: timeout)
    write(data: data, to: peripheral, serviceUUID: BluetoothConstants.ServiceUUIDs.controlService,
          characteristicUUID: BluetoothConstants.ControlServiceCharacteristicUUIDs.command)
    
  }
  
  /**
   Отправляет команду чтения данных на BLE устройство.
  
   - Parameter from: периферийное устройство.
   - Parameter serviceUUID: UUID сервиса, с которого нужно считать данные.
   - Parameter characteristicUUID: UUID характеристики, с которой нужно считать данные.
   */
  private func read(from peripheral: CBPeripheral,
                    serviceUUID: CBUUID, characteristicUUID: CBUUID) {
    
    let service = peripheral.services?.first { $0.uuid == serviceUUID }
    if let characteristic = service?.characteristics?.first(where: { $0.uuid == characteristicUUID }) {
      peripheral.readValue(for: characteristic)
    }
  }
  
  /**
   Отправляет команду записи данных на BLE устройство.
   
   - Parameter data: данные, которые нужно отправить на BLE устройство.
   - Parameter to: периферийное устройство, на которое нужно отправить команду.
   - Parameter serviceUUID: UUID сервиса, в который нужно отправить команду.
   - Parameter characteristicUUID: UUID характеристики, в которую нужно отправить команду.
   */
  private func write(data: Data, to peripheral: CBPeripheral,
                     serviceUUID: CBUUID, characteristicUUID: CBUUID) {
    
    let service = peripheral.services?.first { $0.uuid == serviceUUID }
    if let characteristic = service?.characteristics?.first(where: { $0.uuid == characteristicUUID }) {
      peripheral.writeValue(data, for: characteristic, type: .withResponse)
      loggingService?.log("Writing data: \(data as NSData) into characteristic: \(characteristic.uuid.uuidString)")
    }
  }
  
  /**
   Отправляет данные из команды на периферийное BLE устройство.
   
   - Parameter command: команда для выполнения.
   - Parameter on: периферийное устройство, на которое нужно отправить команду.
   */
  private func execute(command: BluetoothCommand, on peripheral: CBPeripheral) {
    switch command.commandType {
    case .setSignalPower(let signalPowerIndex):
      sendPowerLevelCommand(powerLevel: signalPowerIndex, to: peripheral)
    case .setTimeoutInterval(let timeout):
      sendTimeoutIntervalCommand(timeout: timeout, to: peripheral)
    case .openGate(let wiegand, let accessCode):
      sendAccesssCodeCommand(wiegand: wiegand, accessCode: accessCode, to: peripheral)
    case .readSignalLevel:
      sendCurrentPowerLevelCommand(to: peripheral)
    case .readTimeoutInterval:
      sendCurrentAdvertisingTimeoutCommand(to: peripheral)
    case .readFirmwareVersion:
      sendCurrentFirmwareVersionCommand(to: peripheral)
    }
  }
}

/**
 Команды, отправляемые на BLE устройство
 */
private struct BluetoothCommandData {
  /**
   Создает данные для отправки команды установки уровня мощности на BLE устройстве.
   
   - Parameter powerLevel: индекс силы вещания устройства в массиве возможных значений силы вещания
   для передачи на устройство. Массив возможных значений вещания:
   [-40 дБм, -20 дБм, -16 дБм, -12 дБм, -8 дБм, -4 дБм, 0 дБм, +3 дБм, +4 дБм].
   */
  static func powerLevelCommand(powerLevel: Int) -> Data {
    var powerLevelValue: UInt8 = UInt8(powerLevel)
    var data = NSData(bytes: [0xA0] as [UInt8], length: 1) as Data
    
    let powerLevelData = NSData(bytes: &powerLevelValue, length: MemoryLayout<UInt8>.size) as Data
    data.append(powerLevelData)
    
    return data
  }
  
  /**
   Создает данные для отправки команды открытия на BLE устройстве.
   
   - Parameter wiegandProtocolType: протокол Виганда для передачи на устройство вместе с ключом.
   - Parameter accessCode: ключ для передачи на периферийное BLE устройство.
   */
  static func accessCodeCommand(wiegand: WiegandProtocolType, accessCode: UInt32) -> Data {
    var wiegandValue: UInt8 = UInt8(wiegand.rawValue)
    var data = NSData(bytes: &wiegandValue, length: MemoryLayout<UInt8>.size) as Data
    
    var accessCodeValue = NSSwapHostIntToBig(accessCode)
    let accessCodeData = NSData(bytes: &accessCodeValue, length: MemoryLayout<UInt32>.size) as Data
    
    let range = accessCodeData.startIndex.advanced(by: 1)..<accessCodeData.endIndex
    let accessCode3BytesData = accessCodeData.subdata(in: range)
    
    data.append(accessCode3BytesData)
    
    if wiegand == .thirtyFour {
      let additionalByte = NSData(bytes: [0x4E] as [UInt8], length: 1) as Data
      data.append(additionalByte)
    }
    
    return data
  }
  
  /**
   Создает данные для отправки команды чтения текущего уровня мощности сигнала на BLE устройстве.
   */
  static func currentPowerLevelCommand() -> Data {
    return NSData(bytes: [0xA1] as [UInt8], length: 1) as Data
  }
  
  /**
   Создает данные для отправки команды чтения текущей версии прошивки BLE устройства.
   */
  static func currentFirmwareVersionCommand() -> Data {
    return NSData(bytes: [0xC0] as [UInt8], length: 1) as Data
  }
  
  /**
   Создает данные для отправки команды чтения текущего таймаута вещания на BLE устройства.
   */
  static func currentAdvertisingTimeout() -> Data {
    return NSData(bytes: [0xD1] as [UInt8], length: 1) as Data
  }
  
  /**
   Создает данные для отправки команды установки таймаута вещания на BLE устройстве.
   
   - Parameter timeout: таймаут вещания после открытия, в секундах от 0 до 255.
   */
  static func advertisingTimeout(timeout: Int) -> Data {
    var timeoutValue: UInt8 = UInt8(timeout)
    var data = NSData(bytes: [0xD0] as [UInt8], length: 1) as Data
    
    let timeoutData = NSData(bytes: &timeoutValue, length: MemoryLayout<UInt8>.size) as Data
    data.append(timeoutData)
    
    return data
  }
}
