//
//  BluetoothDeviceInteractionService.swift
//  BLETest
//
//

import Foundation
import CoreBluetooth

protocol BluetoothDeviceInteractionServiceDelegate: class {
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didReceiveDeviceNameChangedEvent name: String?)
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didRequestPeripheralDisconnectWithReconnectAttempt shouldReconnect: Bool,
                                    shouldReadData: Bool,
                                    shouldOpenGate: Bool)
  func peripheralInteractionServiceDidReceiveReadOrWriteError(_ bleInteractionService: BluetoothDeviceInteractionService)
  func peripheralInteractionServiceWillOpenGate(_ bleInteractionService: BluetoothDeviceInteractionService)
  func peripheralInteractionServiceDidOpenGate(_ bleInteractionService: BluetoothDeviceInteractionService)
  func peripheralInteractionServiceDidChangeSignalPower(_ bleInteractionService: BluetoothDeviceInteractionService)
  func peripheralInteractionServiceDidChangeTimeoutInterval(_ bleInteractionService: BluetoothDeviceInteractionService)
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didReadTimeoutInterval timeout: Int)
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didReadSignalPower signalPower: Int)
  func peripheralInteractionService(_ bleInteractionService: BluetoothDeviceInteractionService,
                                    didReadFirmwareVersion firmwareVersion: String)
}

/**
 Сервис взаимодействия с периферийным BLE устройством.
 */
final class BluetoothDeviceInteractionService: NSObject {
  
  // MARK: - Properties
  
  let peripheral: CBPeripheral
  private var commandService: BluetoothCommandService
  private let loggingService: Logging?
  private let wiegandProtocolType: WiegandProtocolType
  private let accessCode: UInt32
  private let signalPowerIndex: Int
  private let timeoutInterval: Int?
  private var shouldUpdateSignalPower: Bool
  private var shouldUpdateTimeoutInterval: Bool
  private let shouldReadInfo: Bool
  private let shouldOpenGate: Bool
  private var commandQueue = BluetoothCommandQueue()
  /**
   Инициализация сервиса.
   
   - Parameter peripheral: периферийное BLE устройство.
   - Parameter config: структура-обёртка, содержащая необходимые для работы сервиса параметры
   - Parameter loggingService: сервис логирования для дебага.
   */
  init(peripheral: CBPeripheral,
       config: BluetoothDeviceInteractionServiceConfig,
       shouldReadInfo: Bool,
       shouldOpenGate: Bool,
       loggingService: Logging? = nil) {
    self.peripheral = peripheral
    self.loggingService = loggingService
    self.shouldReadInfo = shouldReadInfo
    self.shouldOpenGate = shouldOpenGate
    commandService = BluetoothCommandService(loggingService: loggingService)
    
    wiegandProtocolType = config.wiegandProtocolType
    accessCode = config.accessCode
    signalPowerIndex = config.signalPowerIndex
    shouldUpdateSignalPower = config.shouldUpdateSignalPower
    timeoutInterval = config.timeoutInterval
    shouldUpdateTimeoutInterval = config.shouldUpdateTimeoutInterval
    
    super.init()
    
    peripheral.delegate = self
  }
  
  deinit {
    cleanup()
  }
  
  weak var delegate: BluetoothDeviceInteractionServiceDelegate?
  
  // MARK: - Start
  
  /**
   Инициирует взаимодействие с BLE устройством (поиск сервисов и характеристик и отправку необходимых команд)
   */
  func start() {
    if shouldOpenGate {
      delegate?.peripheralInteractionServiceWillOpenGate(self)
    }
    discoverServices(for: peripheral)
  }
  
  // MARK: - Set signal power
  
  /**
   Вызывает метод отправки команды установки уровня мощности вещания, если это необходимо.
   */
  private func enqueueSettingSignalPowerIfNeeded() {
    let delay: TimeInterval = commandQueue.isEmpty ? 0 : 0.5
    let command = BluetoothCommand(commandType: .setSignalPower(signalPowerIndex: signalPowerIndex), delay: delay)
    guard shouldUpdateSignalPower, !commandQueue.contains(commandWithType: command.commandType) else {
      loggingService?.log("Signal power has not been changed, skipping setting it")
      return
    }
    loggingService?.log("Signal power has been changed, will set new one")
    commandQueue.enqueue(command)
  }
  
  // MARK: - Set timeout interval
  
  /**
   Вызывает метод отправки команды установки таймаута вещания, если это необходимо.
   */
  private func enqueueSettingTimeoutIntervalIfNeeded() {
    guard let timeoutInterval = timeoutInterval else { return }

    let delay: TimeInterval = commandQueue.isEmpty ? 0 : 0.5
    let command = BluetoothCommand(commandType: .setTimeoutInterval(timeout: timeoutInterval), delay: delay)
    guard shouldUpdateTimeoutInterval, !commandQueue.contains(commandWithType: command.commandType) else {
      loggingService?.log("Timeout interval has not been changed, skipping setting it")
      return
    }
    loggingService?.log("Timeout interval has been changed, will set new one")
    commandQueue.enqueue(command)
  }
  
  // MARK: - Read data
  
  /**
   Считывает текущую мощность вещания, таймаут вещания и версию прошивки.
   */
  private func enqueueDataReadingCommands() {
    loggingService?.log("Command service is reading initial BLE data")
    commandQueue.enqueue(BluetoothCommand(commandType: .readSignalLevel))
    commandQueue.enqueue(BluetoothCommand(commandType: .readTimeoutInterval))
    commandQueue.enqueue(BluetoothCommand(commandType: .readFirmwareVersion))
  }
  
  // MARK: - Open Gate
  
  /**
   Вызывает метод отправки команды установки уровня мощности вещания, если это необходимо,
   затем метод отправки команды установки таймаута вещания, если это необходимо,
   и затем вызывает метод отправки команды передачи ключа.
   */
  private func enqueueOpenGateCommand() {
    loggingService?.log("Command service is opening gate")
    let delay: TimeInterval = commandQueue.isEmpty ? 0 : 2
    let command = BluetoothCommand(commandType: .openGate(wiegand: wiegandProtocolType, accessCode: accessCode), delay: delay)
    commandQueue.enqueue(command)
  }
  
  // MARK: - Disconnect
  
  /**
   Сообщает делегату о необходимости отключения от устройства.
   
   - Parameter shouldReconnect: флаг, показывающий, нужно ли совершить
     повторную попытку подключения к устройству после отключения.
   */
  func cleanup(shouldReconnect: Bool = false) {
    loggingService?.log("Cleaning up before disconnecting")
    if peripheral.state != .connected {
      return
    }
    
    peripheral.services?.forEach { service in
      service.characteristics?.forEach { characteristic in
        if characteristic.isNotifying {
          peripheral.setNotifyValue(false, for: characteristic)
        }
      }
    }
    
    delegate?.peripheralInteractionService(self, didRequestPeripheralDisconnectWithReconnectAttempt: shouldReconnect,
                                           shouldReadData: shouldReadInfo,
                                           shouldOpenGate: shouldOpenGate)
  }
  
  // MARK: - Discover characteristics
  
  /**
   Инициировать обнаружение сервисов периферийного устройства.
   
   - Parameter peripheral: периферийное BLE устройство.
   */
  private func discoverServices(for peripheral: CBPeripheral) {
    peripheral.discoverServices(BluetoothConstants.ServiceUUIDs.allServices)
  }
  
  /**
   Инициировать обнаружение характеристик периферийного устройства.
   
   - Parameter peripheral: периферийное BLE устройство.
   - Parameter service: сервис, чьи характеристики требуется обнаружить.
   */
  private func discoverCharaceteristics(for peripheral: CBPeripheral, service: CBService) {
    peripheral.discoverCharacteristics(BluetoothConstants.ControlServiceCharacteristicUUIDs.allCharacteristics,
                                       for: service)
  }
  
  /**
   Обработать данные, полученные из характеристики после отправки команды чтения.
   
   - Parameter data: данные, полученные из характеристики.
   - Parameter command: отправленная команда.
   */
  private func handleResponse(_ data: Data, for command: BluetoothCommand) {
    switch command.commandType {
    case .readSignalLevel:
      handleSignalPowerResponse(data)
    case .readTimeoutInterval:
      handleTimeoutIntervalResponse(data)
    case .readFirmwareVersion:
      delegate?.peripheralInteractionService(self, didReadFirmwareVersion: getVersionResponse(from: data))
    case .setTimeoutInterval:
      handleSetTimeoutIntervalResponse(data)
    case .setSignalPower:
      handleSetSignalPowerResponse(data)
    case .openGate:
      delegate?.peripheralInteractionServiceDidOpenGate(self)
    }
    
    if commandQueue.isEmpty {
      if shouldOpenGate, command.commandType != .openGate(wiegand: wiegandProtocolType, accessCode: accessCode) {
        enqueueOpenGateCommand()
      } else if !shouldOpenGate {
        delegate?.peripheralInteractionService(self,
                                               didRequestPeripheralDisconnectWithReconnectAttempt: false,
                                               shouldReadData: false,
                                               shouldOpenGate: false)
      }
    }
  }
  
  /**
   Обработать ответ на установку уровня мощности вещания.
   
   - Parameter data: данные, полученные из характеристики.
   */
  private func handleSetSignalPowerResponse(_ data: Data) {
    guard receivedOkResponse(from: data) else {
      delegate?.peripheralInteractionServiceDidReceiveReadOrWriteError(self)
      cleanup(shouldReconnect: true)
      return
    }
    
    delegate?.peripheralInteractionServiceDidChangeSignalPower(self)
  }
  
  /**
   Обработать ответ на установку таймаута вещания.
   
   - Parameter data: данные, полученные из характеристики.
   */
  private func handleSetTimeoutIntervalResponse(_ data: Data) {
    guard receivedOkResponse(from: data) else {
      delegate?.peripheralInteractionServiceDidReceiveReadOrWriteError(self)
      cleanup(shouldReconnect: true)
      return
    }
    delegate?.peripheralInteractionServiceDidChangeTimeoutInterval(self)
  }
  
  /**
   Обработать ответ на чтение уровня мощности вещания.
   
   - Parameter data: данные, полученные из характеристики.
   */
  private func handleSignalPowerResponse(_ data: Data) {
    let signalPower = getNumericalResponse(from: data)
    loggingService?.log("signal power: \(signalPower)")
    delegate?.peripheralInteractionService(self, didReadSignalPower: signalPower)
  }
  
  /**
   Обработать ответ на чтение таймаута вещания.
   
   - Parameter data: данные, полученные из характеристики.
   */
  private func handleTimeoutIntervalResponse(_ data: Data) {
    let timeout = getNumericalResponse(from: data)
    loggingService?.log("timeout: \(timeout)")
    delegate?.peripheralInteractionService(self, didReadTimeoutInterval: timeout)
  }
  
  /**
   Получить число из данных, прочитанных в характеристике.
   
   - Parameter data: данные, полученные из характеристики.
   */
  private func getNumericalResponse(from data: Data) -> Int {
    let valueByte = data.withUnsafeBytes {
      [UInt8](UnsafeBufferPointer(start: $0.advanced(by: 1), count: 1))
    }.first ?? 0
    return Int(valueByte)
  }
  
  /**
   Получить версию прошивки из данных, прочитанных в характеристике.
   
   - Parameter data: данные, полученные из характеристики.
   */
  private func getVersionResponse(from data: Data) -> String {
    let majorByte = data.withUnsafeBytes {
      [UInt8](UnsafeBufferPointer(start: $0, count: 1))
    }.first
    
    let minorByte = data.withUnsafeBytes {
      [UInt8](UnsafeBufferPointer(start: $0.advanced(by: 1), count: 1))
    }.first
    
    var version = "неизвестно"
    if let major = majorByte, let minor = minorByte {
      version = "\(major).\(minor)"
    }
    return version
  }
  
  /**
   Проверить, что статус отправленной команды ОК.
   
   - Parameter data: данные, полученные из характеристики.
   */
  private func receivedOkResponse(from data: Data) -> Bool {
    return data == NSData(bytes: [0x4F, 0x4B] as [UInt8], length: 2) as Data
  }
}

// MARK: - CBPeripheralDelegate

extension BluetoothDeviceInteractionService: CBPeripheralDelegate {
  func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
    if let error = error {
      loggingService?.log("encountered error while discovering services: \(error)")
      cleanup(shouldReconnect: true)
      return
    }
    
    for service in (peripheral.services ?? []) {
      loggingService?.log("discovered service: \(service.uuid.uuidString)")
      discoverCharaceteristics(for: peripheral, service: service)
    }
  }
  
  func peripheral(_ peripheral: CBPeripheral,
                  didDiscoverCharacteristicsFor service: CBService, error: Error?) {
    guard service.uuid == BluetoothConstants.ServiceUUIDs.controlService else {
      loggingService?.log("Found unexpected service")
      cleanup(shouldReconnect: true)
      return
    }
    
    loggingService?.log("discovered characteristics for service: \(service.uuid.uuidString)")
    
    var characteristicsToDiscover = BluetoothConstants.ControlServiceCharacteristicUUIDs.allCharacteristics
    for characteristic in service.characteristics ?? [] {
      if let index = characteristicsToDiscover.index(where: { $0 == characteristic.uuid }) {
        characteristicsToDiscover.remove(at: index)
      }
    }
    
    guard characteristicsToDiscover.isEmpty else {
      loggingService?.log("Did not find all required characteristics")
      cleanup(shouldReconnect: true)
      return
    }
    
    let notifyingCharacteristic = (service.characteristics ?? []).first {
      $0.uuid == BluetoothConstants.ControlServiceCharacteristicUUIDs.response
    }
    
    if let characteristic = notifyingCharacteristic {
      peripheral.setNotifyValue(true, for: characteristic)
    }
  }
  
  func peripheral(_ peripheral: CBPeripheral, didWriteValueFor characteristic: CBCharacteristic, error: Error?) {
    if let error = error {
      loggingService?.log("error wile writing in characteristic \(characteristic.uuid.uuidString) error: \(error)")
      delegate?.peripheralInteractionServiceDidReceiveReadOrWriteError(self)
      cleanup(shouldReconnect: true)
      return
    }
    loggingService?.log("Successfully written into characteristic: \(characteristic.uuid.uuidString)")
  }
    
  func peripheralDidUpdateName(_ peripheral: CBPeripheral) {
    delegate?.peripheralInteractionService(self, didReceiveDeviceNameChangedEvent: peripheral.name)
  }
  
  func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
    if let error = error {
      loggingService?.log("error wile reading from characteristic \(characteristic.uuid.uuidString) error: \(error)")
      delegate?.peripheralInteractionServiceDidReceiveReadOrWriteError(self)
      cleanup(shouldReconnect: true)
      return
    }
    
    var readData = NSData()
    if let data = characteristic.value {
      readData = data as NSData
    }
    loggingService?.log("Successfully read data: \(readData) from characteristic: \(characteristic.uuid.uuidString)")
    
    DispatchQueue.main.async {
      if let data = characteristic.value, let command = self.commandQueue.dequeue() {
        self.handleResponse(data, for: command)
      }
      self.commandService.executeFirstCommand(in: self.commandQueue, on: peripheral)
    }
  }
  
  func peripheral(_ peripheral: CBPeripheral, didUpdateNotificationStateFor characteristic: CBCharacteristic, error: Error?) {
    if let error = error {
      loggingService?.log("error wile setting notify state for characteristic \(characteristic.uuid.uuidString) error: \(error)")
      cleanup(shouldReconnect: true)
      return
    }
    
    guard characteristic.uuid == BluetoothConstants.ControlServiceCharacteristicUUIDs.response else {
      loggingService?.log("Set notifying on unexpected characteristic")
      cleanup(shouldReconnect: true)
      return
    }
    
    loggingService?.log("set notify value for characteristic: \(characteristic.uuid)")
    
    // т.к. нужные характеристики обнаружены и подписаны для нотификаций, можно инициировать отправку команд на устройство
    if shouldReadInfo {
      enqueueDataReadingCommands()
      enqueueSettingTimeoutIntervalIfNeeded()
      enqueueSettingSignalPowerIfNeeded()
      commandService.executeFirstCommand(in: commandQueue, on: peripheral)
    } else if shouldOpenGate {
      enqueueSettingTimeoutIntervalIfNeeded()
      enqueueSettingSignalPowerIfNeeded()
      enqueueOpenGateCommand()
      commandService.executeFirstCommand(in: commandQueue, on: peripheral)
    }
  }
}
