//
//  BluetoothServiceConfig.swift
//  BLETest
//

import Foundation

/**
 Структура, хранящая параметры для настройки BluetoothService
 */
public struct BluetoothServiceConfig {
  let wiegandProtocolType: WiegandProtocolType
  let accessCode: UInt32
  let automaticallyOpenGate: Bool
  let rssiThreshold: Decibel
  let signalPowerIndex: Int
  let shouldChangeSignalPower: Bool
  let advertisingTimeout: Int?
  let shouldChangeAdvertisingTimeout: Bool
  
  /**
   Инициализация структуры.
   
   - Parameter accessCode: ключ для передачи на периферийное BLE устройство.
   - Parameter rssiThreshold: нижний порог сигнала, при котором следует подключаться к BLE устройству.
   - Parameter automaticallyOpenGate: нужно ли открывать устройство автоматически.
   - Parameter wiegandProtocolType: протокол Виганда для передачи на устройство вместе с ключом.
   - Parameter signalPowerIndex: индекс силы вещания устройства в массиве возможных значений силы вещания
   для передачи на устройство. Массив возможных значений вещания:
   [-40 дБм, -20 дБм, -16 дБм, -12 дБм, -8 дБм, -4 дБм, 0 дБм, +3 дБм, +4 дБм].
   - Parameter advertisingTimeout: таймаут вещания на устройстве от 0 до 255 с.
   - Parameter shouldChangeAdvertisingTimeout: нужно ли обновлять таймаут вещания на устройстве.
   - Parameter shouldUpdateSignalPower: нужно ли обновлять силу вещания на устройстве.
   */
  public init(accessCode: UInt32,
              rssiThreshold: Decibel = BluetoothConstants.defaultRSSIThreshold,
              automaticallyOpenGate: Bool = true,
              wiegandProtocolType: WiegandProtocolType = BluetoothConstants.defaultWiegandProtocolType,
              signalPowerIndex: Int = BluetoothConstants.defaultSignalPowerIndex,
              advertisingTimeout: Int? = nil,
              shouldChangeAdvertisingTimeout: Bool = false,
              shouldChangeSignalPower: Bool = false) {
    self.rssiThreshold = rssiThreshold
    self.automaticallyOpenGate = automaticallyOpenGate
    self.wiegandProtocolType = wiegandProtocolType
    self.accessCode = accessCode
    self.signalPowerIndex = signalPowerIndex
    self.shouldChangeSignalPower = shouldChangeSignalPower
    self.advertisingTimeout = advertisingTimeout
    self.shouldChangeAdvertisingTimeout = shouldChangeAdvertisingTimeout
  }
}
