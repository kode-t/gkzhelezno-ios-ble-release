//
//  BeaconService.swift
//  BLETest
//
//

import Foundation
import CoreLocation

private extension BluetoothConstants {
  static let beaconUUID = UUID(uuidString: "a2070ea6-2555-11e9-ab14-d663bd873d93")
  static let beaconRegionIdentifier = "beacon_monitoring_region_identifier"
  static let beaconMinor: CLBeaconMinorValue = 2
  static let beaconMajor: CLBeaconMajorValue = 1
}

/**
 Делегат BeaconService
 */
public protocol BeaconServiceDelegate: class {
  func beaconServiceDidEnterBeaconRegion(_ beaconService: BeaconServiceProtocol)
  func beaconServiceDidExitBeaconRegion(_ beaconService: BeaconServiceProtocol)
}

/**
 Протокол сервиса мониторинга входа/выхода в/из регионы iBeacon (используется для принудительного просыпания приложения)
 */
public protocol BeaconServiceProtocol {
  var delegate: BeaconServiceDelegate? { get set }
  
  func startMonitoring()
  func stopMonitoring()
}

/**
 Сервис мониторинга входа/выхода в/из регионы iBeacon (используется для принудительного просыпания приложения)
 */
final class BeaconService: NSObject, BeaconServiceProtocol {
  private let locationManager = CLLocationManager()
  private var monitoredBeaconRegions: [CLBeaconRegion] = []
  private (set) var status: CLAuthorizationStatus = .notDetermined
  
  var eventLogger: Logging?
  
  weak public var delegate: BeaconServiceDelegate?
  
  override init() {
    super.init()
    locationManager.delegate = self
    locationManager.requestAlwaysAuthorization()
  }
  
  deinit {
    locationManager.delegate = nil
  }
  
  /**
   Инициировать мониторинг региона iBeacon.
   */
  public func startMonitoring() {
    guard let beaconUUID = BluetoothConstants.beaconUUID else {
      return
    }
    
    let beaconRegion = CLBeaconRegion(proximityUUID: beaconUUID,
                                      major: BluetoothConstants.beaconMajor,
                                      minor: BluetoothConstants.beaconMinor,
                                      identifier: BluetoothConstants.beaconRegionIdentifier)
    startMonitoringForBeaconRegions([beaconRegion])
    
  }
  
  /**
   Остановить мониторинг региона iBeacon.
   */
  public func stopMonitoring() {
    stopMonitoringForBeaconRegions()
  }

  /**
   Инициировать мониторинг регионов iBeacon.
   
   - Parameter regions: массив регионов для мониторинга
   */
  private func startMonitoringForBeaconRegions(_ regions: [CLBeaconRegion]) {
    guard CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) else {
      return
    }
    
    stopMonitoringForBeaconRegions()
    
    monitoredBeaconRegions = []
    for region in regions {
      locationManager.startMonitoring(for: region)
      monitoredBeaconRegions.append(region)
    }
  }
  
  /**
   Остановить мониторинг всех регионов iBeacon.
   */
  private func stopMonitoringForBeaconRegions() {
    guard CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.self) else {
      return
    }
    for region in monitoredBeaconRegions {
      locationManager.stopMonitoring(for: region)
    }
    monitoredBeaconRegions = []
  }
  
}

extension BeaconService: CLLocationManagerDelegate {
  public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
    self.status = status
  }
  
  public func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
    delegate?.beaconServiceDidEnterBeaconRegion(self)
  }
  
  public func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
    delegate?.beaconServiceDidExitBeaconRegion(self)
  }
}
