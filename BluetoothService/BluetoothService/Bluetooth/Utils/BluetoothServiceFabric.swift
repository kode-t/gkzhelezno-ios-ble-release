//
//  BluetoothServiceFabric.swift
//  BluetoothService
//

import Foundation

/**
 Фабрика для создания BluetoothService и BeaconService
 */
public struct BluetoothServiceFabric {
  public static func createBluetoothService(bluetoothServiceConfig: BluetoothServiceConfig,
                                            canReadDeviceSettings: Bool = false,
                                            loggingService: Logging? = nil) -> BluetoothServiceProtocol {
    return BluetoothService(bluetoothServiceConfig: bluetoothServiceConfig, canReadDeviceSettings: canReadDeviceSettings,
                            loggingService: loggingService)
  }
  
  public static func createBeaconService() -> BeaconServiceProtocol {
    return BeaconService()
  }
}
