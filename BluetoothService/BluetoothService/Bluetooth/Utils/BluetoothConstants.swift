//
//  BluetoothConstants.swift
//  BLETest
//

import Foundation
import CoreBluetooth

public typealias Decibel = Double

/**
 Тип протокола Виганда
 */
public enum WiegandProtocolType: Int, Equatable {
  case twentySix = 26, thirtyFour = 34, fourtyTwo = 42
}

/**
 Протокол, закрывающий методы логирования в файл
 */
public protocol Logging {
  func log(_ string: String)
  func delete()
}

/**
 Тип дебаг событий, используется в проигрывателе звуков
 */
public enum EventType {
  case invalidRSSILevelUnitDiscovered, validRSSILevelUnitDiscovered, connectionErrorEncountered,
  charWriteOrResponseErrorEncountered, charResponseReceivedSuccessfully, gateOpeningError, connectingToGate
}

/**
 Протокол, закрывающий методы проигрывателя звуков
 */
public protocol SoundPlaying {
  var isPlayingSound: Bool { get }
  func soundName(forEvent eventType: EventType, includeExtension: Bool) -> String
  func playSound(forEvent eventType: EventType)
}

/**
 Основные константы, используемые в модуле Bluetooth взаимодействия
 */
public struct BluetoothConstants {
  public static let defaultRSSIThreshold: Decibel = -100
  public static let defaultSignalPowerIndex = 6
  public static let defaultWiegandProtocolType = WiegandProtocolType.twentySix
  
  struct ServiceUUIDs {
    static let advertisedService = CBUUID(string: "6D62")
    static let controlService = CBUUID(string: "00001623-1212-EFDE-1523-785FEABCD123")
    static let allServices = [controlService]
  }
  
  struct ControlServiceCharacteristicUUIDs {
    static let command = CBUUID(string: "00001625-1212-EFDE-1523-785FEABCD123")
    static let response = CBUUID(string: "00001624-1212-EFDE-1523-785FEABCD123")
    static let allCharacteristics = [command, response]
  }
  
  static let peripheralKey = "peripheral"
  static let uuidKey = "uuidKey"
  static let nameKey = "nameKey"
  
  static let connectionTimeoutForeground: TimeInterval = 10.0
  static let connectionTimeoutBackground: TimeInterval = 3.0
  static let waitForManualOpenTime: TimeInterval = 10.0
  static let deviceLostInForegroundTimeoutInterval: TimeInterval = 5.0
  static let deviceLostInBackgroundTimeoutInterval: TimeInterval = 20.0
  static let waitingTimeAfterGateOpeningAttempt: TimeInterval = 5
  
  static let deviceInitiatedDisconnectErrorCode = 7
}
