//
//  BluetoothDeviceInteractionServiceConfig.swift
//  BluetoothService
//

import Foundation

/**
 Структура, хранящая параметры для настройки BluetoothDeviceInteractionService
 */
public struct BluetoothDeviceInteractionServiceConfig {
  let wiegandProtocolType: WiegandProtocolType
  let accessCode: UInt32
  let signalPowerIndex: Int
  let timeoutInterval: Int?
  let shouldUpdateSignalPower: Bool
  let shouldUpdateTimeoutInterval: Bool
  
  /**
   Инициализация структуры.
   
   - Parameter wiegandProtocolType: протокол Виганда для передачи на устройство вместе с ключом.
   - Parameter accessCode: ключ для передачи на периферийное BLE устройство.
   - Parameter signalPowerIndex: индекс силы вещания устройства в массиве возможных значений силы вещания
   для передачи на устройство. Массив возможных значений вещания:
   [-40 дБм, -20 дБм, -16 дБм, -12 дБм, -8 дБм, -4 дБм, 0 дБм, +3 дБм, +4 дБм].
   - Parameter timeoutInterval: таймаут вещания на устройстве от 0 до 255 с.
   - Parameter shouldUpdateSignalPower: нужно ли обновлять силу вещания на устройстве.
   - Parameter shouldUpdateTimeoutInterval: нужно ли обновлять таймаут вещания на устройстве.
   - Parameter loggingService: сервис логирования для дебага.
   */
  public init(wiegandProtocolType: WiegandProtocolType,
              accessCode: UInt32,
              signalPowerIndex: Int,
              timeoutInterval: Int?,
              shouldUpdateSignalPower: Bool,
              shouldUpdateTimeoutInterval: Bool) {
    self.wiegandProtocolType = wiegandProtocolType
    self.accessCode = accessCode
    self.signalPowerIndex = signalPowerIndex
    self.timeoutInterval = timeoutInterval
    self.shouldUpdateSignalPower = shouldUpdateSignalPower
    self.shouldUpdateTimeoutInterval = shouldUpdateTimeoutInterval
  }
}
